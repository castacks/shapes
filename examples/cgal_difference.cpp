/* Copyright 2014 Sanjiban Choudhury
 * cgal_difference.cpp
 *
 *  Created on: May 22, 2014
 *      Author: Sanjiban Choudhury
 */


#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <list>
typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
typedef Kernel::Point_2                                   Point_2;
typedef CGAL::Polygon_2<Kernel>                           Polygon_2;
typedef CGAL::Polygon_with_holes_2<Kernel>                Polygon_with_holes_2;
typedef std::list<Polygon_with_holes_2>                   Pwh_list_2;
#include "shapes/print_utils.h"

using namespace ca::print_utils;
int main () {
  // Construct the two input polygons.
  Polygon_2 P;
  P.push_back (Point_2 (0.1, 0.75));
  P.push_back (Point_2 (0.9, 0.75));
  P.push_back (Point_2 (0.9, 1.15));
  P.push_back (Point_2 (0.1, 1.15));
  std::cout << "P = "; print_polygon (P);
  Polygon_2 Q;
  Q.push_back (Point_2 (0, 0));
  Q.push_back (Point_2 (1, 0));
  Q.push_back (Point_2 (1, 1));
  Q.push_back (Point_2 (0, 1));
  std::cout << "Q = "; print_polygon (Q);
  // Compute the intersection of P and Q.
  Pwh_list_2                  diffR;
  Pwh_list_2::const_iterator  it;
  CGAL::difference (Q, P, std::back_inserter(diffR));
  std::cout << "The difference:" << std::endl;
  for (it = diffR.begin(); it != diffR.end(); ++it) {
    std::cout << "--> ";
    print_polygon_with_holes (*it);
  }
  return 0;
}



