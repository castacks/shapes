/* Copyright 2014 Sanjiban Choudhury
 * draw_shape.cpp
 *
 *  Created on: May 22, 2014
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "shapes/shapes.h"
#include "shapes/print_utils.h"
#include <vector>
#include <Eigen/StdVector>
#include <visualization_msgs/Marker.h>
#include <angles/angles.h>

using namespace ca;
using namespace print_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "draw_shape");
  ros::NodeHandle n("~");
  std::vector<double> pt;

  ros::Publisher pub_shape = n.advertise<visualization_msgs::Marker>("shape", 1);
  ros::Duration(1.0).sleep();

  int shape_case = 0;
  if (!n.getParam("shape", shape_case)) {
    ROS_ERROR_STREAM("Set shape parameter");
    return EXIT_FAILURE;
  }

  boost::shared_ptr<Shape> shape_obj;

  switch (shape_case) {
    case 0:
    {
      Circle circle(12, 10, 2);
      shape_obj = boost::shared_ptr<Shape>(new Circle(circle));
      break;
    }
    case 1:
    {
      Rectangle rect(10, 10, 3, 4);
      rect.SetRotation2D(M_PI_4);
      shape_obj = boost::shared_ptr<Shape>(new Rectangle(rect));
      break;
    }
    case 2:
    {
      PolygonWithHole::VectorEigen2d outer(4);
      outer[0] = Eigen::Vector2d(0, 0);
      outer[1] = Eigen::Vector2d(2, 0);
      outer[2] = Eigen::Vector2d(2, 2);
      outer[3] = Eigen::Vector2d(0, 2);
      PolygonWithHole::VectorEigen2d hole(4);
      hole[0] = Eigen::Vector2d(0, 0.5);
      hole[1] = Eigen::Vector2d(2, 0.5);
      hole[2] = Eigen::Vector2d(2, 1.5);
      hole[3] = Eigen::Vector2d(0, 1.5);

      PolygonWithHole poly_hole_obj(outer, hole);
      poly_hole_obj.SetCOM(5,6);
      //poly_hole_obj.SetRotation2D(M_PI_4);
      poly_hole_obj.CentreHoleX(0);
      shape_obj = boost::shared_ptr<Shape>(new PolygonWithHole(poly_hole_obj));
      break;
    }
    case 3:
    {
      PolygonWithHole::VectorEigen2d outer(4);
      outer[0] = Eigen::Vector2d(0, 0);
      outer[1] = Eigen::Vector2d(2, 0);
      outer[2] = Eigen::Vector2d(2, 2);
      outer[3] = Eigen::Vector2d(0, 2);
      PolygonWithHole::VectorEigen2d hole(4);
      hole[0] = Eigen::Vector2d(0, 0.5);
      hole[1] = Eigen::Vector2d(2, 0.5);
      hole[2] = Eigen::Vector2d(2, 1.5);
      hole[3] = Eigen::Vector2d(0, 1.5);

      PolygonWithHoleDepth poly_hole_obj(outer, hole, -0.1, 0.1);
      tf::Quaternion quat;
      quat.setRPY(M_PI_4, 0.0, 0.0);
      poly_hole_obj.set_transform(tf::Transform(quat, tf::Vector3(2, 0, 0)));
      //poly_hole_obj.SetRotation2D(M_PI_4);
      poly_hole_obj.CentreHoleX(0);
      shape_obj = boost::shared_ptr<Shape>(new PolygonWithHoleDepth(poly_hole_obj));
      break;
    }
    case 4:
    {
      std::vector<double> axis(3);
      axis[0] = 0;
      axis[1] = 0;
      axis[2] = 1;

      Cylinder cylinder(5,6,0,2,3,axis);
      shape_obj = boost::shared_ptr<Shape>(new Cylinder(cylinder));
      break;
    }
    case 5:
    {
      Polygon::VectorEigen2d outer(6);
      outer[0] = Eigen::Vector2d(0,0);
      outer[1] = Eigen::Vector2d(1,0);
      outer[2] = Eigen::Vector2d(2,1);
      outer[3] = Eigen::Vector2d(1,2);
      outer[4] = Eigen::Vector2d(0,2);
      outer[5] = Eigen::Vector2d(-1,1);

      Polygon poly(outer);
      tf::Quaternion quat;
      quat.setRPY(M_PI_4, 0.0, 0.0);
      poly.set_transform(tf::Transform(quat, tf::Vector3(2, 0, 0)));
      shape_obj = boost::shared_ptr<Shape>(new Polygon(poly));
      break;
    }
    case 6:
    {
      Polygon::VectorEigen2d outer(6);
      outer[0] = Eigen::Vector2d(0,0);
      outer[1] = Eigen::Vector2d(1,0);
      outer[2] = Eigen::Vector2d(2,1);
      outer[3] = Eigen::Vector2d(1,2);
      outer[4] = Eigen::Vector2d(0,2);
      outer[5] = Eigen::Vector2d(-1,1);

      PolygonDepth poly(outer, 1, 2);
      std::cout<<poly.GetBBox().first.transpose()<<"\n";
      std::cout<<poly.GetBBox().second.transpose()<<"\n";
      shape_obj = boost::shared_ptr<Shape>(new PolygonDepth(poly));
      break;
    }
    case 7:
    {
      Sector sector(12, 10, 2, angles::from_degrees(45), angles::from_degrees(180));
      shape_obj = boost::shared_ptr<Shape>(new Sector(sector));
      break;
    }
    case 8:
    {
      SectorDepth sector_depth(12, 10, 2, angles::from_degrees(45), angles::from_degrees(180), 1, 2);
      shape_obj = boost::shared_ptr<Shape>(new SectorDepth(sector_depth));
      break;
    }
    case 9:
    {
      ObliqueRectangularPrism ob_rect_prism(Eigen::Vector3d(1,2,3), Eigen::Vector3d(10,15,6), 2, 4, 1, 4);
      std::cout<<ob_rect_prism.GetBBox().first.transpose()<<"\n";
      std::cout<<ob_rect_prism.GetBBox().second.transpose()<<"\n";

      shape_obj = boost::shared_ptr<Shape>(new ObliqueRectangularPrism(ob_rect_prism));
      break;
    }
    case 10:
    {
      Pyramid pyramid(Eigen::Vector2d(1,2), 2, 5, 7, 5, 2, 1);
      shape_obj = boost::shared_ptr<Shape>(new Pyramid(pyramid));
      break;
    }
    case 11:
   {
       Cuboid cuboid(1,2,3,4, -5, 6);
       shape_obj = boost::shared_ptr<Shape>(new Cuboid(cuboid));
       break;
   }
    default:
    {
      assert(false);
      break;
    }
  }


  visualization_msgs::Marker m = shape_obj->GetMarker(0, 0, 1, 0.3);
  pub_shape.publish(m);
  ros::Duration(1.0).sleep();
}





