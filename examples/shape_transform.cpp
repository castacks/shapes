/**
 * Copyright (c) 2015 Carnegie Mellon University, Sanjiban Choudhury <sanjiban@cmu.edu>
 *
 * For License information please see the LICENSE file in the root directory.
 *
 */


#include "shapes/shapes.h"
#include "geom_cast/geom_cast.hpp"
#include "tf_utils/tf_utils.h"
using namespace ca;

int main(int argc, char **argv) {
  ros::init(argc, argv, "shape_transform");
  ros::NodeHandle n("~");

  ros::Publisher pub_shape = n.advertise<visualization_msgs::Marker>("original_shape", 1);
  ros::Publisher pub_shape_tf = n.advertise<visualization_msgs::Marker>("transformed_shape", 1);


  tf::Transform affine(tf::createIdentityQuaternion());
  {
    affine.setOrigin(tf::Vector3(-10, -10, -10));
    affine = tf::Transform(tf::Quaternion(tf::Vector3(0, 0, 1), -M_PI/4)) * affine;
  }

  {
    tf::Transform pose(tf::createQuaternionFromYaw(M_PI/2));
    pose.setOrigin(tf::Vector3(11,11,11));
    pose = affine * pose;
    std::cout << ca::point_cast<Eigen::Vector3d>(pose.getOrigin()).transpose() << std::endl;
    std::cout << tf::getYaw(pose.getRotation()) << std::endl;
  }

  {
    Eigen::Vector3d pos(0, 1, 0);
    tf_utils::transformVector3d(affine.inverse(), pos);
    std::cout << pos << std::endl;
  }

  ShapePtr shape;
  int scenario = 4;
  switch (scenario) {
    case 1:
    {
      shape.reset(new ObliqueRectangularPrism( Eigen::Vector3d(10, 10, 10), Eigen::Vector3d(20, 20, 10), 5, 5, 15, 15 ));
      break;
    }
    case 2:
    {
      shape.reset(new Cuboid(10, 10, 16, 18, -5, 25));
      break;
    }
    case 3:
    {
      shape.reset(new SectorDepth(10, 10, 12, -M_PI/8+M_PI/4, M_PI/8+M_PI/4, -5, 25));
      break;
    }
    case 4:
    {
      Polygon::VectorEigen2d vertices(4);
      vertices[0] = Eigen::Vector2d(10, 10);
      vertices[1] = Eigen::Vector2d(25, 10);
      vertices[2] = Eigen::Vector2d(25, 25);
      vertices[3] = Eigen::Vector2d(10, 25);
      shape.reset(new PolygonDepth(vertices, -5, 25));
      break;
    }
  }

  ShapePtr transformed_shape(shape->Clone());
  transformed_shape->ApplyTransform(affine);

  {
    Shape::BBox original_bbox = shape->GetBBox();
    std::cout << "[Original] LB: " << original_bbox.first.transpose() << " UB: " << original_bbox.second.transpose() << std::endl;
    Shape::BBox transformed_bbox = transformed_shape->GetBBox();
    std::cout << "[Transformed] LB: " << transformed_bbox.first.transpose() << " UB: " << transformed_bbox.second.transpose() << std::endl;
  }

  {
    std::cout << "[Original] InShape: " << shape->InShape(Eigen::Vector3d(10, 0, 0)) << std::endl;
    std::cout << "[Transformed] InShape: " << transformed_shape->InShape(Eigen::Vector3d(10, 0, 0)) << std::endl;
  }

  ros::Rate loop_rate(1.0);
  while (ros::ok()) {
    pub_shape.publish(shape->GetMarker(0, 0, 1, 0.3));
    pub_shape_tf.publish(transformed_shape->GetMarker(1, 0, 0, 0.3));
    loop_rate.sleep();
  }
}


