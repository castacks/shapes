/* Copyright 2014 Sanjiban Choudhury
 * cgal_polygon_intersection.cpp
 *
 *  Created on: May 22, 2014
 *      Author: Sanjiban Choudhury
 */

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Boolean_set_operations_2.h>
typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
typedef Kernel::Point_2                                   Point_2;
typedef CGAL::Polygon_2<Kernel>                           Polygon_2;
typedef CGAL::Polygon_with_holes_2<Kernel>                Polygon_with_holes_2;
typedef std::list<Polygon_with_holes_2>                   Pwh_list_2;
#include "shapes/print_utils.h"

using namespace ca::print_utils;

int main () {
  Polygon_2 P;
//  P.push_back (Point_2 (-1,-1));
//  P.push_back (Point_2 (1,-1));
//  P.push_back (Point_2 (1,1));
//  P.push_back (Point_2 (-1,1));
  P.push_back (Point_2 (2000, -200));
  P.push_back (Point_2 (2500, -200));
  P.push_back (Point_2 (2500, 500));
  P.push_back (Point_2 (2000, 500));

  std::cout << "P = "; print_polygon (P);
  Polygon_2 Q;
//  Q.push_back(Point_2 (-3.5,-3.5));
//  Q.push_back(Point_2 (-2.5,-3.5));
//  Q.push_back(Point_2 (-2.5, -2.5));
//  Q.push_back(Point_2 (-3.5, -2.5));
  Q.push_back(Point_2 (4619.98,299.846));
  Q.push_back(Point_2 (4000,299.846));
  //Q.push_back(Point_2 (4000,299.846));
  Q.push_back(Point_2 (2719.98,299.846));
  Q.push_back(Point_2 (2719.98,-300.154));
  Q.push_back(Point_2 (3700,-300.154));
  //Q.push_back(Point_2 (3700,-300.154));
  Q.push_back(Point_2 (3999.99,-300.154));
  //Q.push_back(Point_2 (4000,-300.154));
  //Q.push_back(Point_2 (4000,-300.154));
  //Q.push_back(Point_2 (4000,-300.154));
  //Q.push_back(Point_2 (4000,-300.154));
  Q.push_back(Point_2 (4619.98,-300.154));

  std::cout << "Q = "; print_polygon (Q);
//  if ((CGAL::do_intersect (P, Q)))
//    std::cout << "The two polygons intersect in their interior." << std::endl;
//  else
//    std::cout << "The two polygons do not intersect." << std::endl;

  Pwh_list_2                  intR;
  Pwh_list_2::const_iterator  it;
  CGAL::intersection (P, Q, std::back_inserter(intR));
  std::cout << "The intersection:" << std::endl;
  for (it = intR.begin(); it != intR.end(); ++it) {
    std::cout << "--> ";
    print_polygon_with_holes (*it);
  }


  return 0;
}



