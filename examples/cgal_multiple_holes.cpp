/* Copyright 2014 Sanjiban Choudhury
 * cgal_multiple_holes.cpp
 *
 *  Created on: Jul 21, 2014
 *      Author: Sanjiban Choudhury
 */

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <list>
#include <vector>
typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
typedef Kernel::Point_2                                   Point_2;
typedef CGAL::Polygon_2<Kernel>                           Polygon_2;
typedef CGAL::Polygon_with_holes_2<Kernel>                Polygon_with_holes_2;
typedef std::list<Polygon_with_holes_2>                   Pwh_list_2;
typedef std::list<Polygon_2>                              P_list_2;
#include "shapes/print_utils.h"

using namespace ca::print_utils;
int main () {
  // Construct the two input polygons.
  Polygon_2 P;
  P.push_back (Point_2 (0, 0));
  P.push_back (Point_2 (500, 0));
  P.push_back (Point_2 (500, 500));
  P.push_back (Point_2 (0, 500));
  std::cout << "LZ = "; print_polygon (P);

  P_list_2 Q_set;
  Polygon_2 Q;

  Q.clear();
  Q.push_back (Point_2 (950, 950));
  Q.push_back (Point_2 (1050, 950));
  Q.push_back (Point_2 (1050, 1150));
  Q.push_back (Point_2 (950, 1150));
  std::cout << "NFZ 1= "; print_polygon (Q);
  Q_set.push_back(Q);

  Q.clear();
  Q.push_back (Point_2 (-50, 50));
  Q.push_back (Point_2 ( 50, 50));
  Q.push_back (Point_2 ( 50, 150));
  Q.push_back (Point_2 (-50, 150));
  std::cout << "NFZ 1= "; print_polygon (Q);
  Q_set.push_back(Q);

  /*
  Q.clear();
  Q.push_back (Point_2 ( 250, 450));
  Q.push_back (Point_2 ( 350, 450));
  Q.push_back (Point_2 ( 350, 800));
  Q.push_back (Point_2 ( 250, 800));
  std::cout << "NFZ 2= "; print_polygon (Q);
  Q_set.push_back(Q);

  Q.clear();
  Q.push_back (Point_2 ( 200, 200));
  Q.push_back (Point_2 ( 400, 200));
  Q.push_back (Point_2 ( 400, 400));
  Q.push_back (Point_2 ( 200, 400));
  std::cout << "NFZ 3= "; print_polygon (Q);
  Q_set.push_back(Q);*/

  Polygon_with_holes_2 result;
  result.outer_boundary() = P;
  for (P_list_2::const_iterator it = Q_set.begin(); it != Q_set.end(); it++) {
    Pwh_list_2 diffR;
    CGAL::difference (result, *it, std::back_inserter(diffR));
    result = diffR.front();
  }

  std::cout << "The difference:" << std::endl;
  print_polygon_with_holes(result);

  return 0;
}






