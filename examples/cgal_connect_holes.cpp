/* Copyright 2014 Sanjiban Choudhury
 * cgal_connect_holes.cpp
 *
 *  Created on: May 23, 2014
 *      Author: Sanjiban Choudhury
 */

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <list>
#include <CGAL/connect_holes.h>
#include <CGAL/Partition_traits_2.h>
#include <CGAL/partition_2.h>

typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
typedef Kernel::Point_2                                   Point_2;
typedef CGAL::Polygon_2<Kernel>                           Polygon_2;
typedef CGAL::Polygon_with_holes_2<Kernel>                Polygon_with_holes_2;
typedef std::list<Polygon_with_holes_2>                   Pwh_list_2;
typedef std::list<Point_2>                                Pt_list_2;


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
typedef CGAL::Exact_predicates_inexact_constructions_kernel Kinexact;
typedef CGAL::Partition_traits_2<Kinexact>                  Traits;
typedef Traits::Point_2                                     Point_2_inex;
typedef Traits::Polygon_2                                   Polygon_2_inex;
typedef std::list<Polygon_2_inex>                              Polygon_list;
typedef Polygon_2_inex::Vertex_iterator                        Vertex_iterator;

CGAL::Cartesian_converter<Kernel,Kinexact> converter;
#include "shapes/print_utils.h"

using namespace ca::print_utils;
int main () {
  // Construct the two input polygons.
  Polygon_2 P;
  P.push_back (Point_2 (0.1, 0.5));
  P.push_back (Point_2 (0.9, 0.5));
  P.push_back (Point_2 (0.9, 0.75));
  P.push_back (Point_2 (0.1, 0.75));
  std::cout << "P = "; print_polygon (P);
  Polygon_2 Q;
  Q.push_back (Point_2 (0, 0));
  Q.push_back (Point_2 (1, 0));
  Q.push_back (Point_2 (1, 1));
  Q.push_back (Point_2 (0, 1));
  std::cout << "Q = "; print_polygon (Q);
  // Compute the intersection of P and Q.
  Pwh_list_2                  diffR;
  Pwh_list_2::const_iterator  it;
  CGAL::difference (Q, P, std::back_inserter(diffR));
  std::cout << "The difference:" << std::endl;
  for (it = diffR.begin(); it != diffR.end(); ++it) {
    std::cout << "--> ";
    print_polygon_with_holes (*it);
  }

  Polygon_with_holes_2 poly_hole = *diffR.begin();

  Pt_list_2 connect;
  Pt_list_2::iterator connect_it;

  Polygon_2_inex new_poly;
  CGAL::connect_holes(poly_hole, std::back_inserter(connect));
  std::cout << "The connect iterator:" << std::endl;
  for (connect_it = connect.begin(); connect_it != connect.end(); ++connect_it) {
    std::cout << connect_it->x() <<" "<<connect_it->y() <<"\n";
    new_poly.push_back(converter(*connect_it));
  }

  //  Polygon_list partition_polys;
  //  CGAL::y_monotone_partition_2  (new_poly.vertices_begin(),
  //                                  new_poly.vertices_end(),
  //                                  std::back_inserter(partition_polys));
  //
  //  for (Polygon_list::iterator it = partition_polys.begin(); it != partition_polys.end(); it++) {
  //    std::cout<<"\n Polygon: "<<std::distance(partition_polys.begin(), it);
  //    for (Vertex_iterator pt = it->vertices_begin(); pt != it->vertices_end(); pt++)
  //      std::cout<<"\n Vertex: "<<pt->x() <<" "<<pt->y();
  //  }
  //  assert(CGAL::convex_partition_is_valid_2(polygon.vertices_begin(),
  //                                           polygon.vertices_end(),
  //                                           partition_polys.begin(),
  //                                           partition_polys.end()));
  return 0;
}



