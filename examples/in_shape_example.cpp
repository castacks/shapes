/* Copyright 2014 Sanjiban Choudhury
 * in_shape_example.cpp
 *
 *  Created on: May 15, 2014
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "shapes/shapes.h"
#include "shapes/print_utils.h"
#include <vector>
#include <Eigen/StdVector>
#include <angles/angles.h>


using namespace ca;
using namespace print_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "in_shape_example");
  ros::NodeHandle n("~");
  std::vector<double> pt;

  Circle circle(2, 0, 5);
  circle.SetCOM(Eigen::Vector2d(9,12));
  ROS_INFO_STREAM(" Point in circle? "<<circle.InShape(Eigen::Vector2d(12, 13)));

  Rectangle rect(10, 10, 3, 4);
  ROS_INFO_STREAM(" Point in rect? "<<rect.InShape(Eigen::Vector2d(11, 11)));

  std::vector<double> axis(3);
  axis[0] = 0;
  axis[1] = 0;
  axis[2] = 1;

  Cylinder cylinder(5,6,0,2,3,axis);
  ROS_INFO_STREAM("Point in cylinder? "<<cylinder.InShape(Eigen::Vector3d(7,4,-1)));

  PolygonWithHole::VectorEigen2d outer(4);
  outer[0] = Eigen::Vector2d(0, 0);
  outer[1] = Eigen::Vector2d(2, 0);
  outer[2] = Eigen::Vector2d(2, 2);
  outer[3] = Eigen::Vector2d(0, 2);
  PolygonWithHole::VectorEigen2d hole(4);
  hole[0] = Eigen::Vector2d(0, 0.5);
  hole[1] = Eigen::Vector2d(2, 0.5);
  hole[2] = Eigen::Vector2d(2, 1.5);
  hole[3] = Eigen::Vector2d(0, 1.5);

  PolygonWithHole poly_hole_obj(outer, hole);
  poly_hole_obj.SetCOM(5,6);
  ROS_INFO_STREAM(" Point in poly_with_hole? "<<poly_hole_obj.InShape(Eigen::Vector2d(6, 6.1)));


  PolygonWithHoleDepth poly_hole_depth_obj(outer, hole, -0.1, 0.1);
  tf::Quaternion quat;
  quat.setRPY(M_PI_4, 0.0, 0.0);
  poly_hole_depth_obj.set_transform(tf::Transform(quat, tf::Vector3(2, 0, 0)));
  poly_hole_depth_obj.CentreHoleX(0);
  ROS_INFO_STREAM(" Point in poly_with_hole? "<<poly_hole_depth_obj.InShape(Eigen::Vector3d(3.5, 0.707, 0.707)));

  Sector sector(12, 10, 2, angles::from_degrees(45), angles::from_degrees(180));
  ROS_INFO_STREAM(" Point in sector? "<<sector.InShape(Eigen::Vector2d(12 + 1.9*cos(angles::from_degrees(170)), 10 + 1.9*sin(angles::from_degrees(170)))));

  SectorDepth sector_depth(12, 10, 2, angles::from_degrees(45), angles::from_degrees(180), 1, 2);
  ROS_INFO_STREAM(" Point in sector depth? "<<sector_depth.InShape(Eigen::Vector3d(12 + 1.9*cos(angles::from_degrees(170)), 10 + 1.9*sin(angles::from_degrees(170)), 1.5)));
}




