/* Copyright 2015 Sanjiban Choudhury
 * load_sample_polydepth_set.cpp
 *
 *  Created on: Feb 4, 2015
 *      Author: Sanjiban Choudhury
 */


#include <ros/ros.h>
#include "shapes/shapes.h"
#include "shapes/shape_utils.h"
#include <ros/package.h>

using namespace ca;

int main(int argc, char **argv) {
  ros::init(argc, argv, "load_sample_polydepth_set");
  ros::NodeHandle n("~");

  std::string path = ros::package::getPath("shapes") + "/resources/sample_nfz.bagy";
  std::vector<PolygonDepth> nfz_set;
  shape_utils::LoadPolygonDepthSetFromYAML(path, nfz_set);
  for (auto it : nfz_set)
    std::cout<<it.GetMsg()<<"\n";

}
