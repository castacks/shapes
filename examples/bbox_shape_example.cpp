/* Copyright 2015 Sanjiban Choudhury
 * bbox_shape_example.cpp
 *
 *  Created on: Mar 31, 2015
 *      Author: Sanjiban Choudhury
 */

#include "shapes/shapes.h"

using namespace ca;

int main(int argc, char **argv) {
  ShapeSet vol;
  Polygon::VectorEigen2d outer(6);
  outer[0] = Eigen::Vector2d(0,0);
  outer[1] = Eigen::Vector2d(1,0);
  outer[2] = Eigen::Vector2d(2,1);
  outer[3] = Eigen::Vector2d(1,2);
  outer[4] = Eigen::Vector2d(0,2);
  outer[5] = Eigen::Vector2d(-1,1);

  PolygonDepth poly(outer, 1, 2);
  vol.AddShape(ShapePtr(new PolygonDepth(poly)));

  ObliqueRectangularPrism ob_rect_prism(Eigen::Vector3d(1,2,3), Eigen::Vector3d(4,5,6), 1, 2, 3, 4);
  vol.AddShape(ShapePtr(new ObliqueRectangularPrism(ob_rect_prism)));

  std::cout<<vol.GetBBox().first.transpose()<<"\n";
  std::cout<<vol.GetBBox().second.transpose()<<"\n";

}


