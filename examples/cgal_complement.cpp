/* Copyright 2014 Sanjiban Choudhury
 * cgal_complement.cpp
 *
 *  Created on: May 22, 2014
 *      Author: Sanjiban Choudhury
 */


#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <list>
typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
typedef Kernel::Point_2                                   Point_2;
typedef CGAL::Polygon_2<Kernel>                           Polygon_2;
typedef CGAL::Polygon_with_holes_2<Kernel>                Polygon_with_holes_2;
typedef std::list<Polygon_with_holes_2>                   Pwh_list_2;
#include "shapes/print_utils.h"

using namespace ca::print_utils;
int main (){
  // Construct the two input polygons.
  Polygon_2 P;
  P.push_back (Point_2 (0, 0));
  P.push_back (Point_2 (5, 0));
  P.push_back (Point_2 (5, 5));
  std::cout << "P = "; print_polygon (P);

  Polygon_with_holes_2 Pdash;
  CGAL::complement(P, Pdash);
  std::cout << "The complement: ";print_polygon_with_holes (Pdash);
}



