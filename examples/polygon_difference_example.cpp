/* Copyright 2015 Sanjiban Choudhury
 * polygon_difference_example.cpp
 *
 *  Created on: Jan 30, 2015
 *      Author: Sanjiban Choudhury
 */


#include <ros/ros.h>
#include "shapes/shapes.h"
#include "shapes/shape_utils.h"


using namespace ca;

int main(int argc, char **argv) {
  ros::init(argc, argv, "polygon_difference_example");
  ros::NodeHandle n("~");

  Polygon::VectorEigen2d vertices;
  vertices.emplace_back(0, 0);
  vertices.emplace_back(100, 0);
  vertices.emplace_back(100, 100);
  vertices.emplace_back(0, 100);

  Polygon outer(vertices);

  vertices.clear();
  vertices.emplace_back(20, -20);
  vertices.emplace_back(40, -20);
  vertices.emplace_back(40, 140);
  vertices.emplace_back(20, 140);

  Polygon hole1(vertices);
  std::vector<Polygon> hole_set{hole1};

  Eigen::Vector2d pt(10,50);

  Polygon new_outer;
  std::vector<Polygon> new_hole_set;

  shape_utils::ComputePolygonDifferenceContainingPoint(pt, outer, hole_set, new_outer, new_hole_set);

  std::cout<<"Printing outer: \n";
  for (auto it : new_outer.GetVertices())
    std::cout <<it.x()<<" "<<it.y()<<"\n";

  std::cout<<"Printing hole_set: \n";
  for (auto it2 : new_hole_set) {
    std::cout<<"Printing hole: \n";
    for (auto it : it2.GetVertices())
      std::cout <<it.x()<<" "<<it.y()<<"\n";
  }

}

