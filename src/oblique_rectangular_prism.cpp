/* Copyright 2015 Sanjiban Choudhury
 * oblique_rectangular_prism.cpp
 *
 *  Created on: Jan 23, 2015
 *      Author: Sanjiban Choudhury
 */

#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"
#include <math.h>
#include "shapes/visualization_utils.h"

namespace ca {

ObliqueRectangularPrism::ObliqueRectangularPrism(const Eigen::Vector3d &pivot1, const Eigen::Vector3d &pivot2, double pos_y, double neg_y, double pos_z, double neg_z)
: pos_y_(pos_y),
  neg_y_(neg_y),
  pos_z_(pos_z),
  neg_z_(neg_z) {
  transform_.setOrigin(tf::Vector3(pivot1[0], pivot1[1], pivot1[2]));
  dir_ = pivot2 - pivot1;
}

bool ObliqueRectangularPrism::InShape(const std::vector<double> &pt) const{
  BOOST_ASSERT_MSG(pt.size() == 3, "Expects 3d point");
  std::vector<double> pt_tf(pt);
  tf_utils::transformVector(transform_.inverse(), pt_tf);

  double alpha = (pt_tf[0]*dir_[0] + pt_tf[1]*dir_[1])/(dir_[0]*dir_[0] + dir_[1]*dir_[1]);
  if (alpha >=0 && alpha <=1) {
    bool is_pos_y = ((dir_[0])*(pt_tf[1]) -(dir_[1])*(pt_tf[0])) > 0 ;
    double deviation = sqrt((pt_tf[0] - alpha*dir_[0])*(pt_tf[0] - alpha*dir_[0]) + (pt_tf[1] - alpha*dir_[1])*(pt_tf[1] - alpha*dir_[1]));
    if ((is_pos_y && deviation < pos_y_) ||
        (!is_pos_y && deviation < neg_y_)) {
      if ((pt_tf[2] - alpha*dir_[2] >= 0 && std::abs(pt_tf[2] - alpha*dir_[2]) < pos_z_) ||
          (pt_tf[2] - alpha*dir_[2] < 0  && std::abs(pt_tf[2] - alpha*dir_[2]) < neg_z_))
        return true;
    }
  }
  return false;
}

Shape::BBox ObliqueRectangularPrism::GetBBox() const {
  Eigen::Vector3d dir_pos_y(-dir_[1], dir_[0],0);
  dir_pos_y.normalize();

  std::vector<Eigen::Vector3d> vertices;
  vertices.push_back(pos_y_*dir_pos_y + Eigen::Vector3d(0,0,pos_z_) );
  vertices.push_back(-neg_y_*dir_pos_y +  Eigen::Vector3d(0,0,pos_z_) );
  vertices.push_back(pos_y_*dir_pos_y - Eigen::Vector3d(0,0,neg_z_) );
  vertices.push_back(-neg_y_*dir_pos_y -  Eigen::Vector3d(0,0,neg_z_) );
  vertices.push_back(dir_ -neg_y_*dir_pos_y + Eigen::Vector3d(0,0,pos_z_) );
  vertices.push_back(dir_ +pos_y_*dir_pos_y + Eigen::Vector3d(0,0,pos_z_));
  vertices.push_back(dir_ -neg_y_*dir_pos_y - Eigen::Vector3d(0,0,neg_z_) );
  vertices.push_back(dir_ +pos_y_*dir_pos_y - Eigen::Vector3d(0,0,neg_z_));

  for (auto &it : vertices)
    tf_utils::transformVector3d(transform_, it);

  Eigen::Vector3d lower = vertices.front(), upper = vertices.front();
  for (auto it : vertices) {
    lower[0] = std::min(lower[0], it[0]);
    lower[1] = std::min(lower[1], it[1]);
    lower[2] = std::min(lower[2], it[2]);
    upper[0] = std::max(upper[0], it[0]);
    upper[1] = std::max(upper[1], it[1]);
    upper[2] = std::max(upper[2], it[2]);
  }

  return std::make_pair(lower, upper);
}

visualization_msgs::Marker ObliqueRectangularPrism::GetMarker(double r, double g, double b, double a) {
  visualization_msgs::Marker m;
  m.ns = "oblique_rectangular_prism";
  m.header.stamp = ros::Time::now();
  m.header.frame_id = "/world";
  m.action = visualization_msgs::Marker::ADD;
  m.type = visualization_msgs::Marker::TRIANGLE_LIST;
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;
  m.scale.x = 1.0;
  m.scale.y = 1.0;
  m.scale.z = 1.0;
  tf::quaternionTFToMsg (tf::createIdentityQuaternion(), m.pose.orientation);
  tf_utils::transformPose(transform_, m.pose);

  Eigen::Vector3d dir_pos_y(-dir_[1], dir_[0], 0);
  dir_pos_y.normalize();

  Eigen::Vector3d top1 = pos_y_*dir_pos_y + Eigen::Vector3d(0,0,pos_z_);
  Eigen::Vector3d top2 = -neg_y_*dir_pos_y + Eigen::Vector3d(0,0,pos_z_);
  Eigen::Vector3d bottom1 = pos_y_*dir_pos_y - Eigen::Vector3d(0,0,neg_z_);
  Eigen::Vector3d bottom2 = -neg_y_*dir_pos_y - Eigen::Vector3d(0,0,neg_z_);

  Eigen::Vector3d top3 = dir_ -neg_y_*dir_pos_y + Eigen::Vector3d(0,0,pos_z_);
  Eigen::Vector3d top4 = dir_ +pos_y_*dir_pos_y + Eigen::Vector3d(0,0,pos_z_);
  Eigen::Vector3d bottom3 = dir_ -neg_y_*dir_pos_y - Eigen::Vector3d(0,0,neg_z_);
  Eigen::Vector3d bottom4 = dir_ +pos_y_*dir_pos_y - Eigen::Vector3d(0,0,neg_z_);

  visualization_utils::AddRectangleTriangleList(top1, top2, top3, top4, m);
  visualization_utils::AddRectangleTriangleList(bottom1, bottom2, bottom3, bottom4, m);
  visualization_utils::AddRectangleTriangleList(top1, top2, bottom2, bottom1, m);
  visualization_utils::AddRectangleTriangleList(top3, top4, bottom4, bottom3, m);
  visualization_utils::AddRectangleTriangleList(top2, top3, bottom3, bottom2, m);
  visualization_utils::AddRectangleTriangleList(top1, top4, bottom4, bottom1, m);

  return m;
}

} //namespace ca


