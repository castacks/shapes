/* Copyright 2014 Sanjiban Choudhury
 * polygon_with_hole.cpp
 *
 *  Created on: May 23, 2014
 *      Author: Sanjiban Choudhury
 */

#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"
#include "shapes/visualization_utils.h"

namespace ca {

PolygonWithHolesDepth::PolygonWithHolesDepth(const PolygonDepth &outer, const std::vector<PolygonDepth> &holes)
: outer_(outer),
  holes_(holes){}

bool PolygonWithHolesDepth::InShape (const std::vector<double> &pt) const{
  if (outer_.InShape(pt)) {
    for (std::size_t i = 0; i < holes_.size(); i++)
      if (holes_[i].InShape(pt))
        return false;
    return true;
  } else {
    return false;
  }
}

visualization_msgs::Marker PolygonWithHolesDepth::GetMarker(double r, double g, double b, double a) {
  return outer_.GetMarker(r, g, b, a);
}

shapes::PolygonWithHolesDepth PolygonWithHolesDepth::GetMsg() const {
  shapes::PolygonWithHolesDepth msg;
  msg.outer = outer_.GetMsg();
  for (auto it : holes_)
    msg.holes.push_back(it.GetMsg());
  return msg;
}

void PolygonWithHolesDepth::FromMsg(const shapes::PolygonWithHolesDepth &msg) {
  outer_.FromMsg(msg.outer);
  holes_.clear();
  for (auto it : msg.holes) {
    PolygonDepth h;
    h.FromMsg(it);
    holes_.push_back(h);
  }
}

}  // namespace ca
