/* Copyright 2014 Sanjiban Choudhury
 * rectangle.cpp
 *
 *  Created on: May 23, 2014
 *      Author: Sanjiban Choudhury
 */
#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"

namespace ca {

Rectangle::Rectangle(double xcom, double ycom, double xsize, double ysize)
: rectangle_(Point(-0.5*xsize, -0.5*ysize), Point(0.5*xsize, 0.5*ysize)) {
  transform_.setOrigin(tf::Vector3(xcom,ycom,0));

  width_ = xsize;
  height_ = ysize;
}

bool Rectangle::InShape(const std::vector<double> &pt) const{
  BOOST_ASSERT_MSG(pt.size() == 2, "Expects 2d point");
  std::vector<double> pt_tf(pt);
  tf_utils::transformVector(transform_.inverse(), pt_tf);
  return rectangle_.has_on_bounded_side (Point(pt_tf[0], pt_tf[1]));
}

visualization_msgs::Marker Rectangle::GetMarker(double r, double g, double b, double a) {
  visualization_msgs::Marker m;
  m.ns = "Rectangle";
  m.header.stamp = ros::Time::now();
  m.header.frame_id = "/world";
  m.action = visualization_msgs::Marker::ADD;
  tf::quaternionTFToMsg (tf::createIdentityQuaternion(), m.pose.orientation);
  tf_utils::transformPose(transform_, m.pose);
  m.scale.x = 1.0;
  m.scale.y = 1.0;
  m.scale.z = 1.0;
  geometry_msgs::Point ps;
  for (size_t i = 0; i <=4; i++) {
    ps.x = rectangle_[i].x(); ps.y = rectangle_[i].y();
    m.points.push_back(ps);
    if (i%3==2)
      m.points.push_back(ps);
  }
  m.type = visualization_msgs::Marker::TRIANGLE_LIST;
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;
  return m;
}

void Rectangle::GetSize (double &width, double &height) {
  width = width_;
  height = height_;
}

}  //namespace ca


