/* Copyright 2014 Sanjiban Choudhury
 * shapes.cpp
 *
 *  Created on: May 15, 2014
 *      Author: Sanjiban Choudhury
 */

#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"

namespace ca {

bool Shape::InShape(const Eigen::Vector3d &pt) const {
  return InShape(std::vector<double>(pt.data(), pt.data() + pt.size()));
}

bool Shape::InShape(const Eigen::Vector2d &pt) const {
  return InShape(std::vector<double>(pt.data(), pt.data() + pt.size()));
}

bool Shape::InShape(const Eigen::VectorXd &pt) const {
  return InShape(std::vector<double>(pt.data(), pt.data() + pt.size()));
}

bool Shape::InShape(double x, double y) const {
  std::vector<double> pt;
  pt.push_back(x);
  pt.push_back(y);
  return InShape(pt);
}

bool Shape::InShape(double x, double y, double z) const {
  std::vector<double> pt;
  pt.push_back(x);
  pt.push_back(y);
  pt.push_back(z);
  return InShape(pt);
}

Shape::BBox Shape::GetBBox() const {
  BBox bbox;
  bbox.first = Eigen::VectorXd::Zero(GetDimension());
  bbox.second = bbox.first;
  return bbox;
}

void Shape::SetCOM(const Eigen::Vector2d &pt) {
  transform_.setOrigin(tf::Vector3(pt.x(), pt.y(), 0.0));
}

void Shape::SetCOM(const Eigen::Vector3d &pt) {
  transform_.setOrigin(tf::Vector3(pt.x(), pt.y(), pt.z()));
}

void Shape::SetCOM(double x, double y) {
  transform_.setOrigin(tf::Vector3(x, y, 0.0));
}

void Shape::SetCOM(double x, double y, double z) {
  transform_.setOrigin(tf::Vector3(x, y, z));
}

void Shape::SetCOM(const std::vector<double> &pt) {
  if (pt.size() == 2)
    transform_.setOrigin(tf::Vector3(pt[0], pt[1], 0.0));
  else
    transform_.setOrigin(tf::Vector3(pt[0], pt[1], pt[2]));
}

void Shape::SetRotation2D(double angle) {
  transform_.setRotation(tf::Quaternion(tf::Vector3(0, 0, 1), angle));
}

void Shape::GetRotation2D(double &angle) {
  angle = tf::getYaw(transform_.getRotation());
}

void Shape::GetCOM(std::vector<double> &pt) {
  if (GetDimension() == 2) {
    pt = std::vector<double>(2);
    pt[0] = transform_.getOrigin().x();
    pt[1] = transform_.getOrigin().y();
  } else {
    pt = std::vector<double>(3);
    pt[0] = transform_.getOrigin().x();
    pt[1] = transform_.getOrigin().y();
    pt[2] = transform_.getOrigin().z();
  }
}

template<typename VertexListType, typename PolygonType>
bool Shape::IsValidPolygon(const VertexListType & vertices) {

  //Less than 3 vertice - invalid polygon
  int size = vertices.size();
  if (size < 3) {
    return false;
  }

  //Nan vertex - invalid polygon
  if (Shape::checkNan(vertices)) {
    std::cerr << "ERROR - IsValidPolygon(): vertice is invalid (NaN)." << std::endl;
    return false;
  }

  //Check if polygon is simple
  PolygonType polygon_aux;
  for (auto it : vertices)
    polygon_aux.push_back(
        typename decltype(polygon_aux)::value_type(it.x(), it.y()));
  if (!polygon_aux.is_simple()) {
    std::cerr << "ERROR - IsValidPolygon(): not a simple polygon." << std::endl;
    return false;
  }

  //Repeated vertex - invalid polygon
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      if (i != j) {
        if ((vertices[i].x() == vertices[j].x())
            && (vertices[i].y() == vertices[j].y())) {
          std::cerr << "ERROR - IsValidPolygon(): repeated vertice." << std::endl;
          return false;
        }
      }
    }
  }

  return true;
}

//Specialization of IsValidPolygon for 2D Polygons
template bool Shape::IsValidPolygon<Polygon::VectorEigen2d,Polygon::Polygon_2>(const Polygon::VectorEigen2d & vertices);


}  // namespace ca
