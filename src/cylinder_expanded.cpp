/* Copyright 2015 Sanjiban Choudhury
 * cylinder_expanded.cpp
 *
 *  Created on: Oct 25, 2015
 *      Author: Sanjiban Choudhury
 */

#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"
#include <math.h>


namespace ca {

CylinderExpanded::CylinderExpanded(double x, double y, double z, double r, double exp, double h, const std::vector<double> &ax) {
    std::vector<double> axis(ax);
    double magnitude = sqrt(pow(ax[0], 2) + pow(ax[1], 2) + pow(ax[2], 2));
    if(magnitude!=1) {
        for(int i=0;i<3;i++) {
            axis[i] = axis[i]/magnitude;
        }
    }

    transform_.setOrigin(tf::Vector3(x,y,z));
    double d = sqrt(pow(axis[1], 2) + pow(axis[2], 2));
    tf::Quaternion rotation;
    rotation.setRotation(tf::Vector3(0,1,0), asin(axis[0]));
    if(d!=0) {
        rotation *= (tf::Quaternion(tf::Vector3(1,0,0), -asin(axis[1]/d)));
    }

    transform_.setRotation(rotation);
    height_ = h;
    radius_ = r;
    exp_ = exp;
}

bool CylinderExpanded::InShape(const std::vector<double> &pt) const{
    BOOST_ASSERT_MSG(pt.size() == 3, "Expects 3d point");
    std::vector<double> pt_tf(pt);
    tf_utils::transformVector(transform_.inverse(), pt_tf);
    if(pt_tf[2] > height_/2 || pt_tf[2] < -height_/2) {
        return false;
    }
    else {
        double r = radius_ + exp_;
        Cylinder::Circle2d circle_(Cylinder::Point2d(0,0), Cylinder::K::FT(r*r));
        return circle_.has_on_bounded_side (Cylinder::Point2d(pt_tf[0], pt_tf[1]));
    }
}

visualization_msgs::Marker CylinderExpanded::GetMarker(double r, double g, double b, double a) {
    visualization_msgs::Marker m;
    m.ns = "cylinder";
    m.header.stamp = ros::Time::now();
    m.header.frame_id = "/world";
    m.action = visualization_msgs::Marker::ADD;
    tf::quaternionTFToMsg (tf::createIdentityQuaternion(), m.pose.orientation);
    tf_utils::transformPose(transform_, m.pose);
    m.scale.x = 2*radius_;
    m.scale.y = m.scale.x;
    m.scale.z = height_;
    m.type = visualization_msgs::Marker::CYLINDER;
    m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;
    return m;
}

} //namespace ca


