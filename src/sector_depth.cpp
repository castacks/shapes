/* Copyright 2015 Sanjiban Choudhury
 * sector_depth.cpp
 *
 *  Created on: Jan 22, 2015
 *      Author: Sanjiban Choudhury
 */

#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"


namespace ca {


SectorDepth::SectorDepth(double x, double y, double r, double theta_start, double theta_end, double depth_lower, double depth_upper)
: Sector(x, y, r, theta_start, theta_end),
  depth_lower_(depth_lower),
  depth_upper_(depth_upper){}

bool SectorDepth::InShape (const std::vector<double> &pt) const{
  BOOST_ASSERT_MSG(pt.size() == 3, "Expects 3d point");
  std::vector<double> pt_tf(pt);
  tf_utils::transformVector(transform_.inverse(), pt_tf);
  // VERY VERY VERY IMP: Call Sector::InShape with this argument. Anyother argument calls SectorDepth instead.
  return (pt_tf[2] >= depth_lower_) && (pt_tf[2] <= depth_upper_) && Sector::InShape(std::vector<double>{pt[0], pt[1]} );
}

visualization_msgs::Marker SectorDepth::GetMarker(double r, double g, double b, double a) {
  visualization_msgs::Marker m = Sector::GetMarker(r, g, b, a);
  m.ns = "SectorDepth";
  std::vector<geometry_msgs::Point> points =  m.points;
  m.points.clear();
  for (std::vector<geometry_msgs::Point>::iterator it = points.begin(); it != points.end(); ++it) {
    geometry_msgs::Point ps(*it);
    ps.z = depth_lower_;
    m.points.push_back(ps);
  }

  for (std::vector<geometry_msgs::Point>::iterator it = points.begin(); it != points.end(); ++it) {
    geometry_msgs::Point ps(*it);
    ps.z = depth_upper_;
    m.points.push_back(ps);

  }

  int count = 0;
  std::vector<geometry_msgs::Point> ps_pivot(2);
  for (size_t i = 0; i < points.size(); i++, count ++) {
    if (count % 3 == 0)
      continue;
    ps_pivot[(count % 3) -1] = points[i];
    if (count % 3 == 2) {
      geometry_msgs::Point ps;
      ps = ps_pivot[0];
      ps.z = depth_lower_;
      m.points.push_back(ps);
      ps.z = depth_upper_;
      m.points.push_back(ps);

      ps = ps_pivot[1];
      ps.z = depth_lower_;
      m.points.push_back(ps);

      m.points.push_back(ps);
      ps = ps_pivot[0];
      ps.z = depth_upper_;
      m.points.push_back(ps);
      ps = ps_pivot[1];
      ps.z = depth_upper_;
      m.points.push_back(ps);
    }
  }

  return m;
}

Shape::BBox SectorDepth::GetBBox() const {
  BBox bbox_3d;
  BBox bbox_2d = Sector::GetBBox();
  bbox_3d.first = Eigen::Vector3d(bbox_2d.first.x(), bbox_2d.first.y(),
                                  depth_lower_ + transform_.getOrigin().z());
  bbox_3d.second = Eigen::Vector3d(bbox_2d.second.x(), bbox_2d.second.y(),
                                   depth_upper_ + transform_.getOrigin().z());

  return bbox_3d;
}


}  // namespace ca


