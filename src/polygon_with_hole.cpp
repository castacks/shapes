/* Copyright 2014 Sanjiban Choudhury
 * polygon_with_hole.cpp
 *
 *  Created on: May 23, 2014
 *      Author: Sanjiban Choudhury
 */

#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"
#include "shapes/visualization_utils.h"

namespace ca {

PolygonWithHole::PolygonWithHole(const VectorEigen2d &polygon, const VectorEigen2d &hole)
: polygon_(),
  hole_(){
  polygon_.clear();
  hole_.clear();

  //if not valid polygon return
  if(!IsValidPolygon(polygon)){
    ROS_ERROR_STREAM("PolygonWithHole(): Invalid polygon provided - discarding vertice.");
    return;
  }

  if (polygon.size() > 0) {
    for (VectorEigen2d::const_iterator it = polygon.begin(); it != polygon.end(); ++it)
      polygon_.push_back(Point_2(it->x(), it->y()));
    if (polygon_.orientation() == CGAL::CLOCKWISE)
      polygon_.reverse_orientation();
  }
  if (hole.size() > 0) {
    for (VectorEigen2d::const_iterator it = hole.begin(); it != hole.end(); ++it)
      hole_.push_back(Point_2(it->x(), it->y()));
    if (hole_.orientation() == CGAL::CLOCKWISE)
      hole_.reverse_orientation();
  }
}

void PolygonWithHole::CentreHole(double x, double y) {
  if (hole_.size() == 0) return;
  Bbox2 outer_bounds = hole_.bbox();
  Transformation translate(CGAL::TRANSLATION, Vector(x - 0.5*(outer_bounds.xmin() + outer_bounds.xmax()), y - 0.5*(outer_bounds.ymin() + outer_bounds.ymax())));
  hole_ = CGAL::transform(translate, hole_);
}

void PolygonWithHole::CentreHoleX(double x) {
  if (hole_.size() == 0) return;
  Bbox2 outer_bounds = hole_.bbox();
  Transformation translate(CGAL::TRANSLATION, Vector(x - 0.5*(outer_bounds.xmin() + outer_bounds.xmax()), 0));
  hole_ = CGAL::transform(translate, hole_);
}

void PolygonWithHole::CentreHoleY(double y) {
  if (hole_.size() == 0) return;
  Bbox2 outer_bounds = hole_.bbox();
  Transformation translate(CGAL::TRANSLATION, Vector(0  , y - 0.5*(outer_bounds.ymin() + outer_bounds.ymax())));
  hole_ = CGAL::transform(translate, hole_);
}

bool PolygonWithHole::InShape (const std::vector<double> &pt) const{
  BOOST_ASSERT_MSG(pt.size() == 2, "Expects 2d point");
  std::vector<double> pt_tf(pt);
  tf_utils::transformVector(transform_.inverse(), pt_tf);
  return polygon_.has_on_bounded_side(Point_2(pt_tf[0], pt_tf[1])) && !hole_.has_on_bounded_side(Point_2(pt_tf[0], pt_tf[1]));
}

visualization_msgs::Marker PolygonWithHole::GetMarker(double r, double g, double b, double a) {
  Pwh_list_2 polygon_with_hole_;
  CGAL::difference (polygon_, hole_, std::back_inserter(polygon_with_hole_));
  visualization_msgs::Marker m = ca::visualization_utils::GetMarker(polygon_with_hole_);
  m.ns = "PolygonWithHole";
  m.header.stamp = ros::Time::now();
  m.header.frame_id = "/world";
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;
  tf::quaternionTFToMsg (tf::createIdentityQuaternion(), m.pose.orientation);
  tf_utils::transformPose(transform_, m.pose);
  return m;
}

bool PolygonWithHole::IsValidPolygon(const VectorEigen2d & vertices) {
  return Shape::IsValidPolygon<VectorEigen2d,Polygon_2>(vertices);
}

}  // namespace ca
