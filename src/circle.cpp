/* Copyright 2014 Sanjiban Choudhury
 * circle.cpp
 *
 *  Created on: May 23, 2014
 *      Author: Sanjiban Choudhury
 */

#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"

namespace ca {

Circle::Circle(double x, double y, double r)
: circle_(Circle::Point(0, 0), Circle::K::FT(r*r)) {
  transform_.setOrigin(tf::Vector3(x,y,0));
  radius_ = r;
}

bool Circle::InShape(const std::vector<double> &pt) const{
  BOOST_ASSERT_MSG(pt.size() == 2, "Expects 2d point");
  std::vector<double> pt_tf(pt);
  tf_utils::transformVector(transform_.inverse(), pt_tf);
  return circle_.has_on_bounded_side (Point(pt_tf[0], pt_tf[1]));
}

visualization_msgs::Marker Circle::GetMarker(double r, double g, double b, double a) {
  visualization_msgs::Marker m;
  m.ns = "circle";
  m.header.stamp = ros::Time::now();
  m.header.frame_id = "/world";
  m.action = visualization_msgs::Marker::ADD;
  tf::quaternionTFToMsg (tf::createIdentityQuaternion(), m.pose.orientation);
  tf_utils::transformPose(transform_, m.pose);
  m.scale.x = 2*sqrt(circle_.squared_radius());
  m.scale.y = m.scale.x;
  m.scale.z = 0.1;
  m.type = visualization_msgs::Marker::CYLINDER;
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;
  return m;
}

void Circle::GetSize(double &radius) {
  radius = radius_;
}

}  // namespace ca
