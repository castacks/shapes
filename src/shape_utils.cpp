/* Copyright 2015 Sanjiban Choudhury
 * shape_utils.cpp
 *
 *  Created on: Jan 30, 2015
 *      Author: Sanjiban Choudhury
 */

#include "shapes/shape_utils.h"
#include <CGAL/create_offset_polygons_2.h>
#include <CGAL/Partition_traits_2.h>
#include <CGAL/partition_2.h>

namespace ca {
namespace shape_utils {


bool ComputePolygonDifferenceContainingPoint(const Eigen::Vector2d &pt,  const Polygon &outer, const std::vector<Polygon> &holes, Polygon &new_outer, std::vector<Polygon> &new_holes) {
  Polygon::Polygon_with_holes_2 result;
  Polygon::VectorEigen2d vertices = outer.GetVertices();
  for (auto it : vertices)
    result.outer_boundary().push_back(Polygon::Point_2(it.x(), it.y()));
  for (auto it : holes) {
    std::list<Polygon::Polygon_with_holes_2> diffR;
    Polygon::Polygon_2 hole;
    for (auto it2 : it.GetVertices())
      hole.push_back(Polygon::Point_2(it2.x(), it2.y()));
    CGAL::difference (result, hole, std::back_inserter(diffR));
    bool correct_item = false;
    for (auto pwh : diffR) {
      if (pwh.outer_boundary().has_on_bounded_side(Polygon::Point_2(pt[0], pt[1]))) {
        Polygon::Polygon_with_holes_2::Hole_const_iterator hit;
        correct_item = true;
        for (hit = pwh.holes_begin(); hit != pwh.holes_end(); ++hit) {
          correct_item = correct_item && !hit->has_on_bounded_side(Polygon::Point_2(pt[0], pt[1]));
          if (!correct_item)
            break;
        }
        if (correct_item) {
          result = pwh;
          break;
        }
      }
    }
    if (!correct_item) {
      return false;
    }
  }

  Polygon::VectorEigen2d new_outer_vertices;
  for (size_t i = 0; i < result.outer_boundary().size(); i++)
    new_outer_vertices.emplace_back(CGAL::to_double(result.outer_boundary()[i].x()), CGAL::to_double(result.outer_boundary()[i].y()));
  new_outer = Polygon(new_outer_vertices);

  Polygon::Polygon_with_holes_2::Hole_const_iterator hit;
  for (hit = result.holes_begin(); hit != result.holes_end(); ++hit) {
    Polygon::VectorEigen2d new_hole_vertices;
    for (size_t i = 0; i < (*hit).size(); i++)
      new_hole_vertices.emplace_back(CGAL::to_double((*hit)[i].x()), CGAL::to_double((*hit)[i].y()));
    new_holes.emplace_back(new_hole_vertices);
  }
  return true;
}


std::vector<Polygon> GetPolygonSetFromPolygonDepthSet(const std::vector<PolygonDepth> &poly_depth_set) {
  std::vector<Polygon> poly_set;
  for (auto it : poly_depth_set) {
    Polygon::VectorEigen2d vertices = it.GetUntransformedVertices();
    poly_set.emplace_back(vertices);
  }
  return poly_set;
}


std::vector<PolygonDepth> CreateConvexSubpartPolygonDepthSet(const std::vector<PolygonDepth> &poly_depth_set) {
  typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
  typedef CGAL::Partition_traits_2<K>                         Traits;
  typedef Traits::Point_2                                     Point_2;
  typedef Traits::Polygon_2                                   Polygon_2;
  typedef Polygon_2::Vertex_iterator                          PolyVIterator;
  typedef std::list<Polygon_2>                                Polygon_list;

  std::vector<PolygonDepth> cvx_poly_set;

  for (auto it : poly_depth_set) {
    Polygon::VectorEigen2d vertices = it.GetUntransformedVertices();

    if (PolygonDepth::IsValidPolygon(vertices)) {
      Polygon_2 polygon_in;
      for (auto it2 : vertices) {
        polygon_in.push_back(Point_2(it2.x(), it2.y()));
      }

      if (polygon_in.orientation() == CGAL::CLOCKWISE)
        polygon_in.reverse_orientation();
      Polygon_list  polygon_set_out;
      CGAL::approx_convex_partition_2(polygon_in.vertices_begin(),
                                      polygon_in.vertices_end(),
                                      std::back_inserter(polygon_set_out));
      for (Polygon_list::iterator it2 = polygon_set_out.begin(); it2 != polygon_set_out.end(); it2++) {
        Polygon::VectorEigen2d cvx_vertices;
        for ( PolyVIterator pt = it2->vertices_begin(); pt != it2->vertices_end(); pt++)
          cvx_vertices.emplace_back(CGAL::to_double(pt->x()), CGAL::to_double(pt->y()));
        //Check if new polygon is valid
        if (PolygonDepth::IsValidPolygon(cvx_vertices)) {
          cvx_poly_set.emplace_back(cvx_vertices, it.z_lower(), it.z_upper());
        }
        else {
          ROS_ERROR_STREAM("CreateConvexSubpartPolygonDepthSet - Created polygon is not valid / discarding it.");
        }
      }

    }
    else {
      ROS_ERROR_STREAM("CreateConvexSubpartPolygonDepthSet - Invalid input polygon discarded.");
    }
  }
  return cvx_poly_set;
}


std::vector<PolygonDepth> ExpandPolygonDepthSet(const std::vector<PolygonDepth> &poly_depth_set, double offset) {
  std::vector<PolygonDepth> expanded_poly_set;
  for (auto it : poly_depth_set) {
    Polygon::VectorEigen2d vertices = it.GetUntransformedVertices();
    if (PolygonDepth::IsValidPolygon(vertices)) {
      Polygon::Polygon_2 polygon_in;
      for (auto it2 : vertices)
        polygon_in.push_back(Polygon::Point_2(it2.x(), it2.y()));

      Polygon::PolygonPtrVector outer_offset_polygons;
      if (polygon_in.orientation() == CGAL::CLOCKWISE)
        polygon_in.reverse_orientation();
      Polygon::FT lOffset = offset ;
      outer_offset_polygons = CGAL::create_exterior_skeleton_and_offset_polygons_2(lOffset, polygon_in);

      for (auto it2 : outer_offset_polygons) {
        Polygon::VectorEigen2d expanded_vertices;
        for ( Polygon::PolyVIterator pt = it2->vertices_begin(); pt != it2->vertices_end(); pt++)
          expanded_vertices.emplace_back(CGAL::to_double(pt->x()), CGAL::to_double(pt->y()));
        if (PolygonDepth::IsValidPolygon(expanded_vertices)) {
          expanded_poly_set.emplace_back(expanded_vertices, it.z_lower()-0.2*offset, it.z_upper()+0.2*offset);
        }
        else {
          ROS_ERROR_STREAM("ExpandPolygonDepthSet - Expanded polygon is invalid / discarding it.");
        }
      }
    }
    else {
      ROS_ERROR_STREAM("ExpandPolygonDepthSet - Invalid input polygon discarded.");
    }
  }
  return expanded_poly_set;
}


std::vector<PolygonDepth> EncloseSmallPolygonDepthSet(const std::vector<PolygonDepth> &poly_depth_set, double tolerance) {
  std::vector<PolygonDepth> enclosed_poly_set;
  for (auto it : poly_depth_set) {
    double max_side = 0;
    Polygon::VectorEigen2d vertices = it.GetUntransformedVertices();
    if (PolygonDepth::IsValidPolygon(vertices)) {
      for (size_t i = 0; i < vertices.size() - 1; i++) {
        double length = (vertices[i+1] - vertices[i]).norm();
        if (length > max_side)
          max_side = length;
      }
      if (max_side < tolerance) {
        Eigen::Vector2d centre;
        for (auto it : vertices)
          centre += it;
        centre /= (double)vertices.size();
        double max_dis = 0;
        for (auto it : vertices)
          if ((it - centre).norm() > max_dis)
            max_dis = (it - centre).norm();

        double side = std::max(tolerance, max_dis);
        Polygon::VectorEigen2d new_vertices;
        new_vertices.push_back(centre + Eigen::Vector2d(-side, -side));
        new_vertices.push_back(centre + Eigen::Vector2d(-side, side));
        new_vertices.push_back(centre + Eigen::Vector2d(side, side));
        new_vertices.push_back(centre + Eigen::Vector2d(side, -side));
        enclosed_poly_set.emplace_back(new_vertices, it.z_lower(), it.z_upper());
      } 
      else {
        enclosed_poly_set.push_back(it);
      }
    }
    else {
      ROS_ERROR_STREAM("EncloseSmallPolygonDepthSet - Invalid input polygon discarded.");
    }
  }
  return enclosed_poly_set;
}


bool JoinPolygon(const Polygon &first, const Polygon &second, Polygon &result) {
  Polygon::Polygon_2 first_poly = first.polygon();
  Polygon::Polygon_2 second_poly = second.polygon();

  Polygon::Polygon_with_holes_2 unionR;
  if(!CGAL::join (first_poly, second_poly, unionR))
    return false;

  Polygon::VectorEigen2d new_outer_vertices;
  for (size_t i = 0; i < unionR.outer_boundary().size(); i++)
    new_outer_vertices.emplace_back(CGAL::to_double(unionR.outer_boundary()[i].x()), CGAL::to_double(unionR.outer_boundary()[i].y()));

  if (!Polygon::IsValidPolygon(new_outer_vertices)) {
    return false;
  }

  result = Polygon(new_outer_vertices);
  return true;
}


bool LoadPolygonDepthSetFromYAML(const std::string& filename, std::vector<PolygonDepth> &poly_depth_set) {
  try {
    std::ifstream fin(filename.c_str());
    YAML::Node doc = YAML::Load(fin);
    const YAML::Node& poly_nodes = doc;
    for(size_t i = 0; i < poly_nodes.size(); i++)
      poly_depth_set.emplace_back(poly_nodes[i].as<ca::PolygonDepth>());
  } catch(YAML::ParserException& e) {
    ROS_ERROR_STREAM("YAML loading exception "<<e.what());
    return false;
  }
  return true;
}


bool WritePolygonDepthSetFromYAML(const std::string& filename, const std::vector<PolygonDepth> &poly_depth_set) {
  try {
    std::ofstream fout(filename.c_str());
    YAML::Node poly_nodes;
    for (auto it : poly_depth_set)
      poly_nodes.push_back(it);
    fout << poly_nodes;
  } catch(YAML::ParserException& e) {
    ROS_ERROR_STREAM("YAML loading exception "<<e.what());
    return false;
  }
  return true;
}


std::vector<PolygonDepth> IntersectPolygonDepth(const PolygonDepth &first, const PolygonDepth &second) {
  std::vector<PolygonDepth> result;
  std::pair<double, double> z_intersection { std::max(first.z_lower(), second.z_lower()), std::min(first.z_upper(), second.z_upper()) };
  if (z_intersection.first >= z_intersection.second) {
    return result;
  }

  Polygon::Polygon_2 first_p = first.polygon();
  Polygon::Polygon_2 second_p = second.polygon();

  if ((first_p.size() < 3) || (second_p.size() < 3)) {
    //If one is not a polygon - return empty
    return result;
  }

  if (!CGAL::do_intersect (first_p, second_p)) {
    //Polygons do not intersect - return empty
    return result;
  }

  std::list<Polygon::Polygon_with_holes_2> intR;
  CGAL::intersection (first_p, second_p, std::back_inserter(intR));
  for (auto it : intR) {
    Polygon::VectorEigen2d new_poly_vertices;
    for (std::size_t i = 0; i < it.outer_boundary().size(); i++)
      new_poly_vertices.emplace_back(CGAL::to_double(it.outer_boundary()[i].x()), CGAL::to_double(it.outer_boundary()[i].y()));
    new_poly_vertices = FilterVertices(new_poly_vertices);
    
    if (PolygonDepth::IsValidPolygon(new_poly_vertices)) {
      PolygonDepth new_poly(new_poly_vertices, z_intersection.first, z_intersection.second);
      result.push_back(new_poly);
    }
    else {
      ROS_ERROR_STREAM("IntersectPolygonDepth - New polygon is invalid / discarding it.");
    }
  }
  return result;
}


PolygonDepth JoinPolygonDepth(const PolygonDepth &first, const PolygonDepth &second) {

  if ((first.GetVertices().size() < 3) && (second.GetVertices().size() < 3))
    return PolygonDepth();
  if (first.GetVertices().size() < 3)
    return second;
  else if (second.GetVertices().size() < 3)
    return first;

  std::pair<double, double> z_union { std::min(first.z_lower(), second.z_lower()), std::max(first.z_upper(), second.z_upper()) };
  Polygon::Polygon_2 first_p = first.polygon();
  Polygon::Polygon_2 second_p = second.polygon();
  Polygon::Polygon_with_holes_2 union_p;
  if (! CGAL::join (first_p, second_p, union_p)) {
    ROS_ERROR_STREAM("JoinPolygonDepth - Polygons do not overlap: returning empty polygon.");
    return PolygonDepth();  
  }
  Polygon::VectorEigen2d new_poly_vertices;
  for (std::size_t i = 0; i < union_p.outer_boundary().size(); i++)
    new_poly_vertices.emplace_back(CGAL::to_double(union_p.outer_boundary()[i].x()), CGAL::to_double(union_p.outer_boundary()[i].y()));
  new_poly_vertices = FilterVertices(new_poly_vertices);
  
  if (PolygonDepth::IsValidPolygon(new_poly_vertices)) {
    PolygonDepth new_poly(new_poly_vertices, z_union.first, z_union.second);
    return new_poly;
  }
  else {
    ROS_ERROR_STREAM("JoinPolygonDepth - New polygon is invalid / discarding it (returning empty polygon).");
    return PolygonDepth();
  }
}


Polygon::VectorEigen2d FilterVertices(const Polygon::VectorEigen2d &input) {
  double eps = 1e-3;
  Polygon::VectorEigen2d output;
  for (auto it : input) {
    if (output.size() == 0 || (it - output.back()).norm() > eps)
      output.push_back(it);
  }
  return output;
}

PolygonDepth ConvertCuboidToPolygonDepth(const Cuboid &cuboid) {
  Shape::BBox bbox = cuboid.GetBBox();
  Polygon::VectorEigen2d vertices;
  vertices.push_back(Eigen::Vector2d(bbox.first.x(), bbox.first.y()));
  vertices.push_back(Eigen::Vector2d(bbox.first.x(), bbox.second.y()));
  vertices.push_back(Eigen::Vector2d(bbox.second.x(), bbox.second.y()));
  vertices.push_back(Eigen::Vector2d(bbox.second.x(), bbox.first.y()));
  PolygonDepth poly(vertices, cuboid.z_lower(), cuboid.z_upper());
  return poly;
}

}
}


