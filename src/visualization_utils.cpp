/* Copyright 2014 Sanjiban Choudhury
 * visualization_utils.cpp
 *
 *  Created on: May 23, 2014
 *      Author: Sanjiban Choudhury
 */

#include "shapes/visualization_utils.h"
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Polygon_2.h>
#include <boost/next_prior.hpp>
#include <iostream>
#include "geom_cast/point_cast.hpp"


namespace ca {
namespace visualization_utils {

/**
 * \brief A struct contain face info
 */
struct FaceInfo2 {
  FaceInfo2()
  : nesting_level() {}

  int nesting_level;

  bool in_domain() {
    return nesting_level%2 == 1;
  }
};

typedef CGAL::Exact_predicates_inexact_constructions_kernel       K;
typedef CGAL::Triangulation_vertex_base_2<K>                      Vb;
typedef CGAL::Triangulation_face_base_with_info_2<FaceInfo2,K>    Fbb;
typedef CGAL::Constrained_triangulation_face_base_2<K,Fbb>        Fb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb>               TDS;
typedef CGAL::Exact_predicates_tag                                Itag;
typedef CGAL::Constrained_Delaunay_triangulation_2<K, TDS, Itag>  CDT;
typedef CDT::Point                                                Point;
typedef CGAL::Polygon_2<K>                                        Polygon;
CGAL::Cartesian_converter<Kernel,K> converter;

void mark_domains(CDT& ct, CDT::Face_handle start, int index, std::list<CDT::Edge>& border ) {
  if(start->info().nesting_level != -1) {
    return;
  }
  std::list<CDT::Face_handle> queue;
  queue.push_back(start);

  while(! queue.empty()) {
    CDT::Face_handle fh = queue.front();
    queue.pop_front();
    if(fh->info().nesting_level == -1) {
      fh->info().nesting_level = index;
      for(int i = 0; i < 3; i++) {
        CDT::Edge e(fh,i);
        CDT::Face_handle n = fh->neighbor(i);
        if(n->info().nesting_level == -1) {
          if(ct.is_constrained(e)) border.push_back(e);
          else queue.push_back(n);
        }
      }
    }
  }
}

/**
 * \brief explore set of facets connected with non constrained edges,
 * and attribute to each such set a nesting level.
 * We start from facets incident to the infinite vertex, with a nesting
 * level of 0. Then we recursively consider the non-explored facets incident
 * to constrained edges bounding the former set and increase the nesting level by 1.
 * Facets in the domain are those with an odd nesting level.
 * @param cdt Constrained delanauy triangulation object
 */
void mark_domains(CDT& cdt) {
  for(CDT::All_faces_iterator it = cdt.all_faces_begin(); it != cdt.all_faces_end(); ++it){
    it->info().nesting_level = -1;
  }

  int index = 0;
  std::list<CDT::Edge> border;
  mark_domains(cdt, cdt.infinite_face(), index++, border);
  while(! border.empty()){
    CDT::Edge e = border.front();
    border.pop_front();
    CDT::Face_handle n = e.first->neighbor(e.second);
    if(n->info().nesting_level == -1){
      mark_domains(cdt, n, e.first->info().nesting_level+1, border);
    }
  }
}

void insert_polygon(CDT& cdt,const Polygon& polygon){
  if ( polygon.is_empty() ) return;
  CDT::Vertex_handle v_prev=cdt.insert(*boost::prior(polygon.vertices_end()));
  for (Polygon::Vertex_iterator vit=polygon.vertices_begin();
      vit!=polygon.vertices_end();++vit)
  {
    CDT::Vertex_handle vh=cdt.insert(*vit);
    cdt.insert_constraint(vh,v_prev);
    v_prev=vh;
  }
}



visualization_msgs::Marker GetMarker(const Pwh_list_2 &poly_holes_list) {
  visualization_msgs::Marker m;
  m.ns = "PolygonWithHoles";
  m.header.stamp = ros::Time::now();
  m.header.frame_id = "/world";
  m.action = visualization_msgs::Marker::ADD;
  m.type = visualization_msgs::Marker::TRIANGLE_LIST;
  m.scale.x = 1.0;
  m.scale.y = 1.0;
  m.scale.z = 1.0;
  m.color.r = 1.0;
  m.color.a = 1.0;
  geometry_msgs::Point ps;

  CDT cdt;
  for (Pwh_list_2::const_iterator it = poly_holes_list.begin(); it != poly_holes_list.end(); ++it) {

    Polygon outer;
    for (Vit vit = it->outer_boundary().vertices_begin(); vit != it->outer_boundary().vertices_end(); ++vit)
      outer.push_back(converter(*vit));
    insert_polygon(cdt,outer);
    for (Hit hit = it->holes_begin(); hit != it->holes_end(); ++hit) {
      Polygon hole;
      for (Vit vit = hit->vertices_begin(); vit != hit->vertices_end(); ++vit)
        hole.push_back(converter(*vit));
      if (hole.orientation() == CGAL::CLOCKWISE)
        hole.reverse_orientation();
      insert_polygon(cdt, hole);
    }
  }

  //Mark facets that are inside the domain bounded by the polygon
  mark_domains(cdt);

  for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin(); fit!=cdt.finite_faces_end();++fit) {
    if ( fit->info().in_domain() ) {
      for (size_t i = 0; i < 3; i++) {
        ps.x = fit->vertex(i)->point().x();
        ps.y = fit->vertex(i)->point().y();
        ps.z = 0;
        m.points.push_back(ps);
      }
    }
  }
  return m;
}

visualization_msgs::Marker GetMarker(const Polygon_2 &polygon) {
  visualization_msgs::Marker m;
  m.ns = "Polygon";
  m.header.stamp = ros::Time::now();
  m.header.frame_id = "/world";
  m.action = visualization_msgs::Marker::ADD;
  m.type = visualization_msgs::Marker::TRIANGLE_LIST;
  m.scale.x = 1.0;
  m.scale.y = 1.0;
  m.scale.z = 1.0;
  m.color.r = 1.0;
  m.color.a = 1.0;
  geometry_msgs::Point ps;

  CDT cdt;
  Polygon outer;
  for (Vit vit = polygon.vertices_begin(); vit != polygon.vertices_end(); ++vit) {
    outer.push_back(converter(*vit));
    // outer.push_back(*vit);
  }
  insert_polygon(cdt, outer);

  mark_domains(cdt);

  for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin(); fit!=cdt.finite_faces_end();++fit) {
    if ( fit->info().in_domain() ) {
      for (size_t i = 0; i < 3; i++) {
        ps.x = fit->vertex(i)->point().x();
        ps.y = fit->vertex(i)->point().y();
        ps.z = 0;
        m.points.push_back(ps);
      }
    }
  }
  return m;
}


void AddRectangleTriangleList(const Eigen::Vector3d &pt1, const Eigen::Vector3d &pt2, const Eigen::Vector3d &pt3, const Eigen::Vector3d &pt4, visualization_msgs::Marker &m) {
  m.points.push_back(ca::point_cast<geometry_msgs::Point>(pt1));
  m.points.push_back(ca::point_cast<geometry_msgs::Point>(pt2));
  m.points.push_back(ca::point_cast<geometry_msgs::Point>(pt3));
  m.points.push_back(ca::point_cast<geometry_msgs::Point>(pt3));
  m.points.push_back(ca::point_cast<geometry_msgs::Point>(pt4));
  m.points.push_back(ca::point_cast<geometry_msgs::Point>(pt1));
}


}
}  // namespace ca
