/* Copyright 2014 Sanjiban Choudhury
 * polygon_with_hole.cpp
 *
 *  Created on: June 5th, 2014
 *      Author: Sanjiban Choudhury and Anirudh Vemula
 */

#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"
#include "shapes/visualization_utils.h"
#include <math.h>
#include <vector>

namespace ca {
typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
typedef CGAL::Polygon_2<Kernel>::Vertex_const_iterator Vit;

Polygon::Polygon(const VectorEigen2d &polygon)
    : polygon_() {
  polygon_.clear();

  //if not valid polygon return
  if (!IsValidPolygon(polygon)) {
    ROS_ERROR_STREAM(
        "Polygon(): Invalid polygon provided - discarding vertice.");
    return;
  }

  if (polygon.size() > 0) {
    for (VectorEigen2d::const_iterator it = polygon.begin();
        it != polygon.end(); ++it)
      polygon_.push_back(Point_2(it->x(), it->y()));
    if (polygon_.orientation() == CGAL::CLOCKWISE)
      polygon_.reverse_orientation();
  }
}

bool Polygon::InShape(const std::vector<double> &pt) const {
  BOOST_ASSERT_MSG(pt.size() == 2, "Expects 2d point");
  if (polygon_.size() == 0)
    return false;
  std::vector<double> pt_tf(pt);
  tf_utils::transformVector(transform_.inverse(), pt_tf);
  return polygon_.has_on_bounded_side(Point_2(pt_tf[0], pt_tf[1]));
}

bool Polygon::InShape(const std::vector<double>& pt1,
                      const std::vector<double>& pt2) const {
  //Not valid size
  if (pt1.size() < 2 || pt2.size() < 2) {
    return false;
  }

  //Check if both end points are inside shape
  if (!Polygon::InShape(pt1) || !Polygon::InShape(pt2)) {
    return false;
  }

  //We assume the polygon is convex (CGAL enforced).  As such if two points are inside of it, the line segment drawn between them must also.
  return true;
}

bool Polygon::InShape(const Eigen::Vector2d& pt1,
                      const Eigen::Vector2d& pt2) const {
  std::vector<double> p1(pt1.data(), pt1.data() + pt1.size());
  std::vector<double> p2(pt2.data(), pt2.data() + pt2.size());
  return InShape(p1, p2);
}

bool Polygon::InShape(const Polygon& polygon) const {
  //Assuming convex polygon (CGAL enforced), only need to check if all vertices are inside
  auto verts = polygon.GetVertices();
  for (const auto & v : verts) {
    std::vector<double> p { v.x(), v.y() };
    if (!Polygon::InShape(p)) {
      return false;
    }
  }
  return true;
}

visualization_msgs::Marker Polygon::GetMarker(double r, double g, double b,
                                              double a) {
  visualization_msgs::Marker m = ca::visualization_utils::GetMarker(polygon_);
  m.ns = "Polygon";
  m.header.stamp = ros::Time::now();
  m.header.frame_id = "/world";
  m.color.r = r;
  m.color.g = g;
  m.color.b = b;
  m.color.a = a;
  tf::quaternionTFToMsg(tf::createIdentityQuaternion(), m.pose.orientation);
  tf_utils::transformPose(transform_, m.pose);
  return m;
}

Polygon::VectorEigen2d Polygon::GetVertices() const {
  VectorEigen2d outer;
  for (Vit vit = polygon_.vertices_begin(); vit != polygon_.vertices_end();
      ++vit) {
    std::vector<double> pt { CGAL::to_double(vit->x()), CGAL::to_double(
        vit->y()) };
    tf_utils::transformVector(transform_, pt);
    outer.emplace_back(pt[0], pt[1]);
  }
  return outer;
}

Shape::BBox Polygon::GetBBox() const {
  Polygon::VectorEigen2d vertices = GetVertices();
  Eigen::Vector2d lower = vertices.front(), upper = vertices.front();
  for (auto it : vertices) {
    lower[0] = std::min(lower[0], it[0]);
    lower[1] = std::min(lower[1], it[1]);
    upper[0] = std::max(upper[0], it[0]);
    upper[1] = std::max(upper[1], it[1]);
  }
  return std::make_pair(lower, upper);
}

Polygon::VectorEigen2d Polygon::GetUntransformedVertices() const {
  VectorEigen2d outer;
  for (Vit vit = polygon_.vertices_begin(); vit != polygon_.vertices_end();
      ++vit) {
    std::vector<double> pt { CGAL::to_double(vit->x()), CGAL::to_double(
        vit->y()) };
    outer.emplace_back(pt[0], pt[1]);
  }
  return outer;
}

void Polygon::MakeClockwise() {
  if (polygon_.size() == 0)
    return;
  if (polygon_.orientation() != CGAL::CLOCKWISE)
    polygon_.reverse_orientation();
}

void Polygon::MakeAntiClockwise() {
  if (polygon_.size() == 0)
    return;
  if (polygon_.orientation() == CGAL::CLOCKWISE)
    polygon_.reverse_orientation();
}

void Polygon::ShiftPolygon(const Eigen::Vector2d &offset) {
  for (Vit vit = polygon_.vertices_begin(); vit != polygon_.vertices_end();
      ++vit)
    *vit = Point_2(vit->x() + offset.x(), vit->y() + offset.y());
}

std::ostream& operator<<(std::ostream& os, const Polygon& poly) {
  for (auto it : poly.GetVertices())
    os << it.transpose() << "\n";
  return os;
}

bool Polygon::intersectsShape(const std::vector<double>& pt1,
                              const std::vector<double>& pt2) const {

  //Not valid size
  if (pt1.size() < 2 || pt2.size() < 2) {
    return false;
  }

  //Check if both points are the same
  if (pt1[0] == pt2[0] && pt1[1] == pt2[1]) {
    return InShape(pt1);
  }

  //Check if either end point is inside shape
  if (Polygon::InShape(pt1) || Polygon::InShape(pt2)) {
    return true;
  }

  //Create query segment
  Point_2 p1(pt1.at(0), pt1.at(1));
  Point_2 p2(pt2.at(0), pt2.at(1));
  Segment_2 seg(p1, p2);

  //Check if query segment intersects with any other edge in shape
  for (auto it = polygon_.edges_begin(); it != polygon_.edges_end(); ++it) {
    if (CGAL::do_intersect((*it), seg)) {
      return true;
    }
  }

  return false;
}

bool Polygon::intersectsShape(const Eigen::Vector2d& pt1,
                              const Eigen::Vector2d& pt2) const {
  std::vector<double> p1(pt1.data(), pt1.data() + pt1.size());
  std::vector<double> p2(pt2.data(), pt2.data() + pt2.size());
  return intersectsShape(p1, p2);
}

bool ca::Polygon::intersectsShape(const Polygon& polygon) const {
  auto vert_list = polygon.GetVertices();
  vert_list.push_back(vert_list.front());
  for (size_t i = 0; i < vert_list.size() - 1; i++) {
    std::vector<double> pt1 { vert_list.at(i).x(), vert_list.at(i).y() };
    std::vector<double> pt2 { vert_list.at(i + 1).x(), vert_list.at(i + 1).y() };
    if (Polygon::intersectsShape(pt1, pt2)) {
      return true;
    }
  }
  return false;
}

bool Polygon::IsValidPolygon(const VectorEigen2d & vertices) {
  return Shape::IsValidPolygon<VectorEigen2d, Polygon_2>(vertices);
}

}  //namespace ca
