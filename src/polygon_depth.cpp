/* Copyright 2014 Sanjiban Choudhury
 * polygon_depth.cpp
 *
 *  Created on: Oct 21, 2014
 *      Author: Sanjiban Choudhury
 */

#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"
#include "shapes/visualization_utils.h"

namespace ca {

PolygonDepth::PolygonDepth(const VectorEigen2d &polygon, double z_lower,
                           double z_upper)
    : Polygon(polygon),
      z_lower_(z_lower),
      z_upper_(z_upper) {
}

bool PolygonDepth::InShape(const std::vector<double> &pt) const {
  BOOST_ASSERT_MSG(pt.size() == 3, "Expects 3d point");
  if (polygon_.size() == 0)
    return false;
  std::vector<double> pt_tf(pt);
  tf_utils::transformVector(transform_.inverse(), pt_tf);
  return (pt_tf[2] >= z_lower_) && (pt_tf[2] <= z_upper_)
      && polygon_.has_on_bounded_side(Point_2(pt_tf[0], pt_tf[1]));
}

bool PolygonDepth::InShape(const std::vector<double>& pt1,
                           const std::vector<double>& pt2) const {
  //Not valid size
  if (pt1.size() < 3 || pt2.size() < 3) {
    return false;
  }

  Eigen::Vector3d p1(pt1[0], pt1[1], pt1[2]);
  Eigen::Vector3d p2(pt2[0], pt2[1], pt2[2]);

  //Check that both endpoints are between extreme heights of polygon
  double min_height = std::min(z_lower_, z_upper_);
  double max_height = std::max(z_lower_, z_upper_);
  if (!(p1.z() <= max_height && p1.z() >= min_height)
      || !(p2.z() <= max_height && p2.z() >= min_height)) {
    return false;
  }

  //Check if projection of segment is in shape (just neglect z in this case)
  std::vector<double> pt1_2d { p1[0], p1[1] };
  std::vector<double> pt2_2d { p2[0], p2[1] };
  return Polygon::InShape(pt1_2d, pt2_2d);
}

bool PolygonDepth::InShape(const Eigen::Vector3d& pt1,
                           const Eigen::Vector3d& pt2) const {
  std::vector<double> p1(pt1.data(), pt1.data() + pt1.size());
  std::vector<double> p2(pt2.data(), pt2.data() + pt2.size());
  return InShape(p1, p2);
}

bool PolygonDepth::InShape(const PolygonDepth& polygon) const {
  //Check that at other polygon is within height bounds
  double min_height1 = std::min(z_lower_, z_upper_);
  double max_height1 = std::max(z_lower_, z_upper_);
  double min_height2 = std::min(polygon.z_lower(), polygon.z_upper());
  double max_height2 = std::max(polygon.z_lower(), polygon.z_upper());
  if (!(max_height2 <= max_height1 && min_height2 >= min_height1)) {
    return false;
  }

  //If so check it like regular polygon
  return Polygon::InShape(polygon);

}

bool PolygonDepth::intersectsShape(const std::vector<double>& pt1,
                                   const std::vector<double>& pt2) const {
  //Not valid size
  if (pt1.size() < 3 || pt2.size() < 3) {
    return false;
  }
  Eigen::Vector3d p1(pt1[0], pt1[1], pt1[2]);
  Eigen::Vector3d p2(pt2[0], pt2[1], pt2[2]);

  //Check that at least one of the end points is between extreme heights of polygon
  double min_height = std::min(z_lower_, z_upper_);
  double max_height = std::max(z_lower_, z_upper_);
  if (!(p1.z() <= max_height && p1.z() >= min_height)
      && !(p2.z() <= max_height && p2.z() >= min_height)) {
    return false;
  }

  //Find interpolation scalars between end points that are in extreme heights of polygon
  double alpha1 = (min_height - p2.z()) / (p1.z() - p2.z());
  double alpha2 = (max_height - p2.z()) / (p1.z() - p2.z());

  //Bound them so that points are still on segment
  alpha1 = std::min(1.0, std::max(alpha1, 0.0));
  alpha2 = std::min(1.0, std::max(alpha2, 0.0));

  //Get projections of segment endpoints from alphas
  auto p1_proj = alpha1 * p1 + (1 - alpha1) * p2;
  auto p2_proj = alpha2 * p1 + (1 - alpha2) * p2;

  //Check if projection of segment intersects with shape
  std::vector<double> pt1_2d { p1_proj[0], p1_proj[1] };
  std::vector<double> pt2_2d { p2_proj[0], p2_proj[1] };
  return Polygon::intersectsShape(pt1_2d, pt2_2d);

}

bool PolygonDepth::intersectsShape(const Eigen::Vector3d& pt1,
                                   const Eigen::Vector3d& pt2) const {
  std::vector<double> p1(pt1.data(), pt1.data() + pt1.size());
  std::vector<double> p2(pt2.data(), pt2.data() + pt2.size());
  return intersectsShape(p1, p2);
}

bool PolygonDepth::intersectsShape(const PolygonDepth& polygon) const {

  //Check that at other polygon is within height bounds
  double min_height1 = std::min(z_lower_, z_upper_);
  double max_height1 = std::max(z_lower_, z_upper_);
  double min_height2 = std::min(polygon.z_lower(), polygon.z_upper());
  double max_height2 = std::max(polygon.z_lower(), polygon.z_upper());
  if ((min_height2 >= max_height1 || max_height2 <= min_height1)) {
    return false;
  }

  //If so check it like regular polygon
  return Polygon::intersectsShape(polygon);

}

Shape::BBox PolygonDepth::GetBBox() const {
  BBox bbox_3d;
  if (polygon_.size() > 0) {
    BBox bbox_2d = Polygon::GetBBox();
    bbox_3d.first = Eigen::Vector3d(bbox_2d.first.x(), bbox_2d.first.y(),
                                    z_lower_ + transform_.getOrigin().z());
    bbox_3d.second = Eigen::Vector3d(bbox_2d.second.x(), bbox_2d.second.y(),
                                     z_upper_ + transform_.getOrigin().z());
  }
  return bbox_3d;
}

void PolygonDepth::ShiftPolygonDepth(const Eigen::Vector3d &offset) {
  z_lower_ += offset.z();
  z_upper_ += offset.z();
  Polygon::ShiftPolygon(Eigen::Vector2d(offset.x(), offset.y()));
}

visualization_msgs::Marker PolygonDepth::GetMarker(double r, double g, double b,
                                                   double a) {
  visualization_msgs::Marker m = Polygon::GetMarker(r, g, b, a);
  m.ns = "PolygonDepth";
  std::vector<geometry_msgs::Point> points = m.points;
  m.points.clear();
  for (std::vector<geometry_msgs::Point>::iterator it = points.begin();
      it != points.end(); ++it) {
    geometry_msgs::Point ps(*it);
    ps.z = z_lower_;
    m.points.push_back(ps);
  }

  for (std::vector<geometry_msgs::Point>::iterator it = points.begin();
      it != points.end(); ++it) {
    geometry_msgs::Point ps(*it);
    ps.z = z_upper_;
    m.points.push_back(ps);

  }

  Polygon::VectorEigen2d vertices = GetUntransformedVertices();
  for (size_t it = 0; it < vertices.size(); it++) {
    geometry_msgs::Point ps;
    ps.x = vertices[it][0];
    ps.y = vertices[it][1];
    ps.z = z_lower_;
    m.points.push_back(ps);
    ps.x = vertices[it][0];
    ps.y = vertices[it][1];
    ps.z = z_upper_;
    m.points.push_back(ps);
    ps.x = vertices[(it + 1) % vertices.size()][0];
    ps.y = vertices[(it + 1) % vertices.size()][1];
    ps.z = z_lower_;
    m.points.push_back(ps);

    ps.x = vertices[(it + 1) % vertices.size()][0];
    ps.y = vertices[(it + 1) % vertices.size()][1];
    ps.z = z_lower_;
    m.points.push_back(ps);
    ps.x = vertices[it][0];
    ps.y = vertices[it][1];
    ps.z = z_upper_;
    m.points.push_back(ps);
    ps.x = vertices[(it + 1) % vertices.size()][0];
    ps.y = vertices[(it + 1) % vertices.size()][1];
    ps.z = z_upper_;
    m.points.push_back(ps);
  }
  return m;
}

shapes::PolygonDepth PolygonDepth::GetMsg() const {
  Polygon::VectorEigen2d vertices = GetUntransformedVertices();
  shapes::PolygonDepth msg;
  for (auto it : vertices) {
    shapes::Point2D pt;
    pt.x = it[0];
    pt.y = it[1];
    msg.polygon.push_back(pt);
  }
  msg.z_lower = z_lower_;
  msg.z_upper = z_upper_;
  return msg;
}

void PolygonDepth::FromMsg(const shapes::PolygonDepth &msg) {
  polygon_.clear();
  if (msg.polygon.size() == 0)
    return;
  for (auto it : msg.polygon)
    polygon_.push_back(Point_2(it.x, it.y));

  if (polygon_.orientation() == CGAL::CLOCKWISE)
    polygon_.reverse_orientation();

  z_lower_ = std::min(msg.z_lower, msg.z_upper);
  z_upper_ = std::max(msg.z_lower, msg.z_upper);
}

}  // namespace ca

