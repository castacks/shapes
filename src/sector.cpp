/* Copyright 2015 Sanjiban Choudhury
 * sector.cpp
 *
 *  Created on: Jan 22, 2015
 *      Author: Sanjiban Choudhury
 */


#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"
#include <angles/angles.h>
namespace ca {

Sector::Sector(double x, double y, double r, double theta_start, double theta_end)
: radius_(r),
  theta_start_(theta_start),
  theta_end_(theta_end),
  resolution_(angles::from_degrees(10.0)){
  transform_.setOrigin(tf::Vector3(x,y,0));
}

bool Sector::InShape(const std::vector<double> &pt) const{
  BOOST_ASSERT_MSG(pt.size() == 2, "Expects 2d point");
  std::vector<double> pt_tf(pt);
  tf_utils::transformVector(transform_.inverse(), pt_tf);
  double delta = angles::normalize_angle_positive (theta_end_ - theta_start_);
  double query_delta = angles::normalize_angle_positive (atan2(pt_tf[1], pt_tf[0]) - theta_start_);
  return ((pt_tf[0]*pt_tf[0] + pt_tf[1]*pt_tf[1]) < radius_*radius_) && (query_delta < delta);
}

visualization_msgs::Marker Sector::GetMarker(double r, double g, double b, double a) {
  visualization_msgs::Marker m;
  m.ns = "sector";
  m.header.stamp = ros::Time::now();
  m.header.frame_id = "/world";
  m.action = visualization_msgs::Marker::ADD;
  m.type = visualization_msgs::Marker::TRIANGLE_LIST;
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;
  m.scale.x = 1.0;
  m.scale.y = 1.0;
  m.scale.z = 1.0;
  tf::quaternionTFToMsg (tf::createIdentityQuaternion(), m.pose.orientation);
  tf_utils::transformPose(transform_, m.pose);

  geometry_msgs::Point ps;
  double delta = angles::normalize_angle_positive (theta_end_ - theta_start_);
  int num_slices = std::max(1, (int)std::ceil(delta/resolution_));
  for (int slice = 0; slice < num_slices; slice ++) {
    ps.x = 0;
    ps.y = 0;
    ps.z = 0;
    m.points.push_back(ps);
    double ang1 = theta_start_ + ((double)slice / (double)num_slices)*delta;
    double ang2 = theta_start_ + ((double)(slice+1) / (double)num_slices)*delta;
    ps.x = radius_*cos(ang1);
    ps.y = radius_*sin(ang1);
    ps.z = 0;
    m.points.push_back(ps);

    ps.x = radius_*cos(ang2);
    ps.y = radius_*sin(ang2);
    ps.z = 0;
    m.points.push_back(ps);
  }
  return m;
}

void Sector::GetSize(double &radius) {
  radius = radius_;
}


Shape::BBox Sector::GetBBox() const {
  std::vector<Eigen::Vector2d> vertices;
  vertices.push_back(Eigen::Vector2d(-radius_, -radius_));
  vertices.push_back(Eigen::Vector2d(radius_, -radius_));
  vertices.push_back(Eigen::Vector2d(radius_, radius_));
  vertices.push_back(Eigen::Vector2d(-radius_, radius_));

  for (auto &it : vertices) {
    std::vector<double> pt = {it.x(), it.y()};
    tf_utils::transformVector(transform_, pt);
    it.x() = pt[0]; it.y() = pt[1];
  }

  Eigen::Vector2d lower = vertices.front(), upper = vertices.front();
  for (auto it : vertices) {
    lower[0] = std::min(lower[0], it[0]);
    lower[1] = std::min(lower[1], it[1]);
    upper[0] = std::max(upper[0], it[0]);
    upper[1] = std::max(upper[1], it[1]);
  }

  return std::make_pair(lower, upper);
}


}  // namespace ca

