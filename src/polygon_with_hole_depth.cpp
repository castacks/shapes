/* Copyright 2014 Sanjiban Choudhury
 * polygon_with_hole_depth.cpp
 *
 *  Created on: May 26, 2014
 *      Author: Sanjiban Choudhury
 */

#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"
#include "shapes/visualization_utils.h"

namespace ca {

PolygonWithHoleDepth::PolygonWithHoleDepth(const VectorEigen2d &polygon, const VectorEigen2d &hole, double depth_lower, double depth_upper)
: PolygonWithHole(polygon, hole),
  depth_lower_(depth_lower),
  depth_upper_(depth_upper){}

bool PolygonWithHoleDepth::InShape (const std::vector<double> &pt) const{
  BOOST_ASSERT_MSG(pt.size() == 3, "Expects 3d point");
  std::vector<double> pt_tf(pt);
  tf_utils::transformVector(transform_.inverse(), pt_tf);
  return (pt_tf[2] >= depth_lower_) && (pt_tf[2] <= depth_upper_) && polygon_.has_on_bounded_side(Point_2(pt_tf[0], pt_tf[1])) && !hole_.has_on_bounded_side(Point_2(pt_tf[0], pt_tf[1]));
}

visualization_msgs::Marker PolygonWithHoleDepth::GetMarker(double r, double g, double b, double a) {
  visualization_msgs::Marker m = PolygonWithHole::GetMarker(r, g, b, a);
  m.ns = "PolygonWithHoleDepth";
  std::vector<geometry_msgs::Point> points =  m.points;
  m.points.clear();
  for (std::vector<geometry_msgs::Point>::iterator it = points.begin(); it != points.end(); ++it) {
    geometry_msgs::Point ps(*it);
    ps.z = depth_lower_;
    m.points.push_back(ps);
  }

  for (std::vector<geometry_msgs::Point>::iterator it = points.begin(); it != points.end(); ++it) {
    geometry_msgs::Point ps(*it);
    ps.z = depth_upper_;
    m.points.push_back(ps);
  }
  return m;
}


}  // namespace ca

