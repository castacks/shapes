/* Copyright 2015 Sanjiban Choudhury
 * pyramid.cpp
 *
 *  Created on: Feb 22, 2015
 *      Author: Sanjiban Choudhury
 */

#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"
#include <math.h>
#include "shapes/visualization_utils.h"


namespace ca {

Pyramid::Pyramid(const Eigen::Vector2d &base_centre, double z_low, double z_high, double x_size_base,  double y_size_base, double x_size_top,  double y_size_top)
: base_centre_(base_centre),
  z_low_(z_low),
  z_high_(z_high),
  x_size_base_(x_size_base),
  y_size_base_(y_size_base),
  x_size_top_(x_size_top),
  y_size_top_(y_size_top) {
}

bool Pyramid::InShape(const std::vector<double> &pt) const{
  BOOST_ASSERT_MSG(pt.size() == 3, "Expects 3d point");
  std::vector<double> pt_tf(pt);
  tf_utils::transformVector(transform_.inverse(), pt_tf);
  if(pt_tf[2] > z_high_ || pt_tf[2] < z_low_) {
    return false;
  }
  else {
    double alpha = (pt_tf[2] - z_low_)/(z_high_ - z_low_);
    if ( fabs(pt_tf[0] - base_centre_.x()) <= 0.5*alpha*x_size_top_ + 0.5*(1-alpha)*x_size_base_
        && fabs(pt_tf[1] - base_centre_.y()) <= 0.5*alpha*y_size_top_ + 0.5*(1-alpha)*y_size_base_)
      return true;
    else
      return false;
  }
}

visualization_msgs::Marker Pyramid::GetMarker(double r, double g, double b, double a) {
  visualization_msgs::Marker m;
  m.ns = "pyramid";
  m.header.stamp = ros::Time::now();
  m.header.frame_id = "/world";
  m.action = visualization_msgs::Marker::ADD;
  m.type = visualization_msgs::Marker::TRIANGLE_LIST;
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;
  m.scale.x = 1.0;
  m.scale.y = 1.0;
  m.scale.z = 1.0;
  tf::quaternionTFToMsg (tf::createIdentityQuaternion(), m.pose.orientation);
  tf_utils::transformPose(transform_, m.pose);


  Eigen::Vector3d bottom1(base_centre_.x() - 0.5*x_size_base_, base_centre_.y() - 0.5*y_size_base_, z_low_);
  Eigen::Vector3d bottom2(base_centre_.x() + 0.5*x_size_base_, base_centre_.y() - 0.5*y_size_base_, z_low_);
  Eigen::Vector3d bottom3(base_centre_.x() + 0.5*x_size_base_, base_centre_.y() + 0.5*y_size_base_, z_low_);
  Eigen::Vector3d bottom4(base_centre_.x() - 0.5*x_size_base_, base_centre_.y() + 0.5*y_size_base_, z_low_);

  Eigen::Vector3d top1(base_centre_.x() - 0.5*x_size_top_, base_centre_.y() - 0.5*y_size_top_, z_high_);
  Eigen::Vector3d top2(base_centre_.x() + 0.5*x_size_top_, base_centre_.y() - 0.5*y_size_top_, z_high_);
  Eigen::Vector3d top3(base_centre_.x() + 0.5*x_size_top_, base_centre_.y() + 0.5*y_size_top_, z_high_);
  Eigen::Vector3d top4(base_centre_.x() - 0.5*x_size_top_, base_centre_.y() + 0.5*y_size_top_, z_high_);

  visualization_utils::AddRectangleTriangleList(bottom1, bottom2, bottom3, bottom4, m);
  visualization_utils::AddRectangleTriangleList(top1, top2, top3, top4, m);
  visualization_utils::AddRectangleTriangleList(top1, top2, bottom2, bottom1, m);
  visualization_utils::AddRectangleTriangleList(top3, top4, bottom4, bottom3, m);
  visualization_utils::AddRectangleTriangleList(top2, top3, bottom3, bottom2, m);
  visualization_utils::AddRectangleTriangleList(top1, top4, bottom4, bottom1, m);

  return m;
}

} //namespace ca



