/* Copyright 2015 Sanjiban Choudhury
 * cuboid.cpp
 *
 *  Created on: Mar 14, 2015
 *      Author: Sanjiban Choudhury
 */


#include "shapes/shapes.h"
#include <ros/ros.h>
#include <boost/assert.hpp>
#include "tf_utils/tf_utils.h"
#include "shapes/visualization_utils.h"

namespace ca {


Cuboid::Cuboid(double xcom, double ycom, double xsize, double ysize, double z_lower, double z_upper)
: Rectangle(xcom, ycom, xsize, ysize),
  z_lower_(z_lower),
  z_upper_(z_upper){
  lb_ = Eigen::Vector3d(xcom - 0.5*xsize, ycom - 0.5*ysize, z_lower);
  ub_ = Eigen::Vector3d(xcom + 0.5*xsize, ycom + 0.5*ysize, z_upper);
}

bool Cuboid::InShape (const std::vector<double> &pt) const{
  BOOST_ASSERT_MSG(pt.size() == 3, "Expects 3d point");
  std::vector<double> pt_tf(pt);
  tf_utils::transformVector(transform_.inverse(), pt_tf);
  return (pt_tf[2] >= z_lower_) && (pt_tf[2] <= z_upper_) && rectangle_.has_on_bounded_side (Point(pt_tf[0], pt_tf[1]));
}

visualization_msgs::Marker Cuboid::GetMarker(double r, double g, double b, double a) {
  visualization_msgs::Marker m;
  m.ns = "cuboid";
  m.header.stamp = ros::Time::now();
  m.header.frame_id = "/world";
  m.action = visualization_msgs::Marker::ADD;
  m.type = visualization_msgs::Marker::TRIANGLE_LIST;
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;
  m.scale.x = 1.0;
  m.scale.y = 1.0;
  m.scale.z = 1.0;
  tf::quaternionTFToMsg (tf::createIdentityQuaternion(), m.pose.orientation);
  tf_utils::transformPose(transform_, m.pose);


  Eigen::Vector3d bottom1(rectangle_[0].x(), rectangle_[0].y(), z_lower_);
  Eigen::Vector3d bottom2(rectangle_[1].x(), rectangle_[1].y(), z_lower_);
  Eigen::Vector3d bottom3(rectangle_[2].x(), rectangle_[2].y(), z_lower_);
  Eigen::Vector3d bottom4(rectangle_[3].x(), rectangle_[3].y(), z_lower_);

  Eigen::Vector3d top1(rectangle_[0].x(), rectangle_[0].y(), z_upper_);
  Eigen::Vector3d top2(rectangle_[1].x(), rectangle_[1].y(), z_upper_);
  Eigen::Vector3d top3(rectangle_[2].x(), rectangle_[2].y(), z_upper_);
  Eigen::Vector3d top4(rectangle_[3].x(), rectangle_[3].y(), z_upper_);

  visualization_utils::AddRectangleTriangleList(bottom1, bottom2, bottom3, bottom4, m);
  visualization_utils::AddRectangleTriangleList(top1, top2, top3, top4, m);
  visualization_utils::AddRectangleTriangleList(top1, top2, bottom2, bottom1, m);
  visualization_utils::AddRectangleTriangleList(top3, top4, bottom4, bottom3, m);
  visualization_utils::AddRectangleTriangleList(top2, top3, bottom3, bottom2, m);
  visualization_utils::AddRectangleTriangleList(top1, top4, bottom4, bottom1, m);

  return m;
}

Shape::BBox Cuboid::GetBBox() const {
  std::vector<Eigen::Vector3d> vertices;
  vertices.push_back(Eigen::Vector3d(rectangle_[0].x(), rectangle_[0].y(), z_lower_));
  vertices.push_back(Eigen::Vector3d(rectangle_[1].x(), rectangle_[1].y(), z_lower_));
  vertices.push_back(Eigen::Vector3d(rectangle_[2].x(), rectangle_[2].y(), z_lower_));
  vertices.push_back(Eigen::Vector3d(rectangle_[3].x(), rectangle_[3].y(), z_lower_));

  vertices.push_back(Eigen::Vector3d(rectangle_[0].x(), rectangle_[0].y(), z_upper_));
  vertices.push_back(Eigen::Vector3d(rectangle_[1].x(), rectangle_[1].y(), z_upper_));
  vertices.push_back(Eigen::Vector3d(rectangle_[2].x(), rectangle_[2].y(), z_upper_));
  vertices.push_back(Eigen::Vector3d(rectangle_[3].x(), rectangle_[3].y(), z_upper_));

  for (auto &it : vertices)
    tf_utils::transformVector3d(transform_, it);

  Eigen::Vector3d lower = vertices.front(), upper = vertices.front();
  for (auto it : vertices) {
    lower[0] = std::min(lower[0], it[0]);
    lower[1] = std::min(lower[1], it[1]);
    lower[2] = std::min(lower[2], it[2]);
    upper[0] = std::max(upper[0], it[0]);
    upper[1] = std::max(upper[1], it[1]);
    upper[2] = std::max(upper[2], it[2]);
  }

  return std::make_pair(lower, upper);
}

}  // namespace ca


