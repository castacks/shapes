/* Copyright 2015 Sanjiban Choudhury
 * shapes_test.cpp
 *
 *  Created on: Jan 23, 2015
 *      Author: Sanjiban Choudhury
 */


// Bring in gtest
#include <gtest/gtest.h>
#include <ros/ros.h>
#include <ros/package.h>
#include "shapes/shapes.h"
#include <angles/angles.h>

using namespace ca;

TEST(IsPointInShape, circle_test) {
  Circle circle(2, 0, 5);
  circle.SetCOM(Eigen::Vector2d(9,12));
  ASSERT_TRUE(circle.InShape(Eigen::Vector2d(12, 13))) << "Should be in circle";
}

TEST(IsPointInShape, rect_test) {
  Rectangle rect(10, 10, 3, 4);
  ASSERT_TRUE(rect.InShape(Eigen::Vector2d(11, 11))) << "Should be in rect";
}

TEST(IsPointInShape, cylinder_test) {
  std::vector<double> axis(3);
  axis[0] = 0;
  axis[1] = 0;
  axis[2] = 1;

  Cylinder cylinder(5,6,0,2,3,axis);
  ASSERT_TRUE(cylinder.InShape(Eigen::Vector3d(5,5,1))) << "Should be in cylinder";
}

TEST(IsPointInShape, poly_hole_test) {
  PolygonWithHole::VectorEigen2d outer(4);
  outer[0] = Eigen::Vector2d(0, 0);
  outer[1] = Eigen::Vector2d(2, 0);
  outer[2] = Eigen::Vector2d(2, 2);
  outer[3] = Eigen::Vector2d(0, 2);
  PolygonWithHole::VectorEigen2d hole(4);
  hole[0] = Eigen::Vector2d(0, 0.5);
  hole[1] = Eigen::Vector2d(2, 0.5);
  hole[2] = Eigen::Vector2d(2, 1.5);
  hole[3] = Eigen::Vector2d(0, 1.5);

  PolygonWithHole poly_hole_obj(outer, hole);
  poly_hole_obj.SetCOM(5,6);
  ASSERT_TRUE(poly_hole_obj.InShape(Eigen::Vector2d(6, 6.1))) << "Should be in poly_hole";
}

TEST(IsPointInShape, poly_gole_depth_test) {
  PolygonWithHole::VectorEigen2d outer(4);
  outer[0] = Eigen::Vector2d(0, 0);
  outer[1] = Eigen::Vector2d(2, 0);
  outer[2] = Eigen::Vector2d(2, 2);
  outer[3] = Eigen::Vector2d(0, 2);
  PolygonWithHole::VectorEigen2d hole(4);
  hole[0] = Eigen::Vector2d(0, 0.5);
  hole[1] = Eigen::Vector2d(2, 0.5);
  hole[2] = Eigen::Vector2d(2, 1.5);
  hole[3] = Eigen::Vector2d(0, 1.5);

  PolygonWithHoleDepth poly_hole_depth_obj(outer, hole, -0.1, 0.1);
  tf::Quaternion quat;
  quat.setRPY(M_PI_4, 0.0, 0.0);
  poly_hole_depth_obj.set_transform(tf::Transform(quat, tf::Vector3(2, 0, 0)));
  poly_hole_depth_obj.CentreHoleX(0);
  ASSERT_TRUE(poly_hole_depth_obj.InShape(Eigen::Vector3d(3.5, 0.707, 0.707))) << "Should be in poly_hole_depth";
}


TEST(IsPointInShape, sector_test) {
  Sector sector(12, 10, 2, angles::from_degrees(45), angles::from_degrees(180));
  ASSERT_TRUE(sector.InShape(Eigen::Vector2d(12 + 1.9*cos(angles::from_degrees(170)), 10 + 1.9*sin(angles::from_degrees(170))))) << "Should be in sector";
}

TEST(IsPointInShape, sector_depth_test) {
  SectorDepth sector_depth(12, 10, 2, angles::from_degrees(45), angles::from_degrees(180), 1, 2);
  ASSERT_TRUE(sector_depth.InShape(Eigen::Vector3d(12 + 1.9*cos(angles::from_degrees(170)), 10 + 1.9*sin(angles::from_degrees(170)), 1.5))) << "Should be in sector depth";
}

// Run all the tests that were declared with TEST()
int main(int argc, char **argv) {
  ros::init(argc, argv, "shapes_test");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}





