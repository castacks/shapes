/* Copyright 2014 Sanjiban Choudhury
 * print_utils.h
 *
 *  Created on: May 22, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef INCLUDE_SHAPES_PRINT_UTILS_H_
#define INCLUDE_SHAPES_PRINT_UTILS_H_

#include <CGAL/Polygon_with_holes_2.h>
#include <iostream>

namespace ca{
namespace print_utils{

/**
 * \brief Pretty print a CGAL polygon in std out stream
 * @param P A CGAL polygon
 */
template<class Kernel, class Container>
void print_polygon (const CGAL::Polygon_2<Kernel, Container>& P) {
  typename CGAL::Polygon_2<Kernel, Container>::Vertex_const_iterator  vit;

  std::cout << "[ " << P.size() << " vertices:";
  for (vit = P.vertices_begin(); vit != P.vertices_end(); ++vit)
    std::cout << " (" << *vit << ')';
  std::cout << " ]" << std::endl;

  return;
}


/**
 * \brief Pretty print a CGAL polygon with holes in std out stream
 * @param pwh A CGAL polygon with holes
 */
template<class Kernel, class Container>
void print_polygon_with_holes(const CGAL::Polygon_with_holes_2<Kernel, Container>& pwh) {
  if (! pwh.is_unbounded()) {
    std::cout << "{ Outer boundary = ";
    print_polygon (pwh.outer_boundary());
  }
  else
    std::cout << "{ Unbounded polygon." << std::endl;

  typename CGAL::Polygon_with_holes_2<Kernel,Container>::
                                             Hole_const_iterator  hit;
  unsigned int                                                     k = 1;

  std::cout << "  " << pwh.number_of_holes() << " holes:" << std::endl;
  for (hit = pwh.holes_begin(); hit != pwh.holes_end(); ++hit, ++k) {
    std::cout << "    Hole #" << k << " = ";
    print_polygon (*hit);
  }
  std::cout << " }" << std::endl;

  return;
}

}  // namespace print_utils
}  // namespace ca

#endif  // INCLUDE_SHAPES_PRINT_UTILS_H_ 
