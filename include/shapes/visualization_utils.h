/* Copyright 2014 Sanjiban Choudhury
 * visualization_utils.h
 *
 *  Created on: May 23, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef INCLUDE_SHAPES_VISUALIZATION_UTILS_H_
#define INCLUDE_SHAPES_VISUALIZATION_UTILS_H_

#include <CGAL/Polygon_with_holes_2.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <visualization_msgs/Marker.h>
#include <Eigen/StdVector>

namespace ca {
namespace visualization_utils {

typedef CGAL::Exact_predicates_exact_constructions_kernel       Kernel;
typedef CGAL::Polygon_with_holes_2<Kernel>                      Polygon_with_holes_2;
typedef CGAL::Polygon_with_holes_2<Kernel>::Hole_const_iterator Hit;
typedef std::list<Polygon_with_holes_2>                         Pwh_list_2;
typedef CGAL::Polygon_2<Kernel>::Vertex_const_iterator          Vit;
typedef CGAL::Polygon_2<Kernel>                                 Polygon_2;

/**
 * \brief Create a marker for list of CGAL polygon with holes
 * @param poly_holes_list List of polygon with holes
 * @return Marker that can visualize this list
 */
visualization_msgs::Marker GetMarker(const Pwh_list_2 &poly_holes_list);

/**
 * \brief Create a marker for a given CGAL Polygon
 * @param CGAL Polygon
 * @return Marker that can visualize this polygon
 */
visualization_msgs::Marker GetMarker(const Polygon_2 &polygon);


void AddRectangleTriangleList(const Eigen::Vector3d &pt1, const Eigen::Vector3d &pt2, const Eigen::Vector3d &pt3, const Eigen::Vector3d &pt4, visualization_msgs::Marker &m);

}  // namespace visualization_utils
}  // namespace ca
#endif  // INCLUDE_SHAPES_VISUALIZATION_UTILS_H_
