/* Copyright 2015 Sanjiban Choudhury
 * shape_utils.h
 *
 *  Created on: Jan 30, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef SHAPES_INCLUDE_SHAPES_SHAPE_UTILS_H_
#define SHAPES_INCLUDE_SHAPES_SHAPE_UTILS_H_

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <list>
#include <vector>
#include "shapes/shapes.h"
#include "std_msgs_util/std_msgs_util.h"

namespace YAML {

template<> struct convert<ca::PolygonDepth> {
  static Node encode(const ca::PolygonDepth& poly) {
    Node node;
    Node pt_set;

    for (size_t i = 0; i < poly.GetVertices().size(); i++) {
      Node pt;
      pt["x"] = poly.GetVertices()[i].x();
      pt["y"] = poly.GetVertices()[i].y();
      pt_set[i] = pt;
    }

    node["polygon"] = pt_set;
    node["z_lower"] = poly.z_lower();
    node["z_upper"] = poly.z_upper();
    return node;
  }

  static bool decode(const Node& node, ca::PolygonDepth& poly) {
    const YAML::Node& pt_set = node["polygon"];

    ca::PolygonDepth::VectorEigen2d vert;
    for(size_t i = 0; i < pt_set.size(); i++) {
      double x = pt_set[i]["x"].as<double>();
      double y = pt_set[i]["y"].as<double>();
      vert.emplace_back(x,y);
    }

    double z_lower = node["z_lower"].as<double>();
    double z_upper = node["z_upper"].as<double>();

    poly = ca::PolygonDepth(vert, z_lower, z_upper);

    return true;
  }
};

template<> struct convert<ca::PolygonWithHolesDepth> {
  static Node encode(const ca::PolygonWithHolesDepth& poly) {
    Node node;
    node["outer"] = poly.outer();
    Node hole_set;
    for (size_t i = 0; i < poly.holes().size(); i++) {
       hole_set[i] = poly.holes()[i];
    }
    node["holes"] = hole_set;
    return node;
  }

  static bool decode(const Node& node, ca::PolygonWithHolesDepth& poly) {
    poly.SetOuter(node["outer"].as<ca::PolygonDepth>());
    for (size_t i = 0; i < node["holes"].size(); i++) {
       poly.AddHole(node["holes"][i].as<ca::PolygonDepth>());
    }
    return true;
  }
};

}


namespace ca {
namespace shape_utils {

bool ComputePolygonDifferenceContainingPoint(const Eigen::Vector2d &pt,  const Polygon &outer, const std::vector<Polygon> &holes, Polygon &new_outer, std::vector<Polygon> &new_holes);

std::vector<Polygon> GetPolygonSetFromPolygonDepthSet(const std::vector<PolygonDepth> &poly_depth_set);

bool LoadPolygonDepthSetFromYAML(const std::string& filename, std::vector<PolygonDepth> &poly_depth_set);

bool WritePolygonDepthSetFromYAML(const std::string& filename, const std::vector<PolygonDepth> &poly_depth_set);

std::vector<PolygonDepth> CreateConvexSubpartPolygonDepthSet(const std::vector<PolygonDepth> &poly_depth_set);

std::vector<PolygonDepth> ExpandPolygonDepthSet(const std::vector<PolygonDepth> &poly_depth_set, double offset);

std::vector<PolygonDepth> EncloseSmallPolygonDepthSet(const std::vector<PolygonDepth> &poly_depth_set, double tolerance);

bool JoinPolygon(const Polygon &first, const Polygon &second, Polygon &result);

std::vector<PolygonDepth> IntersectPolygonDepth(const PolygonDepth &first, const PolygonDepth &second);

PolygonDepth JoinPolygonDepth(const PolygonDepth &first, const PolygonDepth &second);

Polygon::VectorEigen2d FilterVertices(const Polygon::VectorEigen2d &input);

PolygonDepth ConvertCuboidToPolygonDepth(const Cuboid &cuboid);

}
}

#endif  // SHAPES_INCLUDE_SHAPES_SHAPE_UTILS_H_ 
