/* Copyright 2014 Sanjiban Choudhury
 * shapes.h
 *
 *  Created on: May 15, 2014
 *      Author: Sanjiban Choudhury and Sankalp Arora
 */

#ifndef SHAPES_SHAPES_H_
#define SHAPES_SHAPES_H_

#include <vector>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/intersections.h>
#include <CGAL/Circle_2.h>
#include <CGAL/Iso_rectangle_2.h>
#include <CGAL/Bbox_2.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <Eigen/StdVector>
#include <tf/tf.h>
#include <tf_conversions/tf_eigen.h>
#include <iostream>

#include "shapes/shape_types.h"
#include "shapes/PolygonDepth.h"
#include "shapes/PolygonWithHolesDepth.h"

namespace ca {

/**
 * \brief An \b abstract shape class which for all purposes is essentially wrapper for CGAL primitives
 * Ideally shapes should be useful in creating custom metashapes which are a combination
 * of simple CGAL primitives.
 */
class Shape {
 public:
  typedef std::pair<Eigen::VectorXd, Eigen::VectorXd> BBox;
  /**
   * \brief Constructor of abstract class. Only used to initialize transform.
   */
  Shape()
      : transform_(tf::createIdentityQuaternion()) {
  }

  virtual ~Shape() {
  }

  /**
   * \brief Gets the dimension of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const = 0;

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() = 0;

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (2d / 3d orr nd depending on shape)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const = 0;
  virtual bool InShape(const Eigen::Vector3d &pt) const;
  virtual bool InShape(const Eigen::Vector2d &pt) const;
  virtual bool InShape(const Eigen::VectorXd &pt) const;
  virtual bool InShape(double x, double y) const;
  virtual bool InShape(double x, double y, double z) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape.
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r, double g, double b,
                                               double a) = 0;

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() = 0;

  virtual BBox GetBBox() const;

  /**
   * \brief Set the centre of mass of the shape to a point
   * @param pt coordinates of COM
   */
  void SetCOM(const std::vector<double> &pt);
  void SetCOM(const Eigen::Vector2d &pt);
  void SetCOM(const Eigen::Vector3d &pt);
  void SetCOM(double x, double y);
  void SetCOM(double x, double y, double z);

  /**
   * \brief Sets the rotation of the shape around the Z axis to an angle
   * @param angle Angle around Z axis
   */
  void SetRotation2D(double angle);

  /**
   * \brief Gets the rotation done to the shape around the Z axis
   * @param angle Angle around Z axis
   */
  void GetRotation2D(double &angle);

  /**
   * Writes the centre of mass of the shape to the reference
   * @param pt COM is written to this variable
   */
  void GetCOM(std::vector<double> &pt);

  /**
   * Returns the current transform of the shape
   * @return the transform of the shape
   */
  tf::Transform transform() const{
    return transform_;
  }

  /**
   * Sets the transform of the shape
   * @param transform The tf to set for the shape
   */
  void set_transform(const tf::Transform &transform) {
    transform_ = transform;
  }

  /**
   * Apply transform to underlying transform
   * @param transform The tf to apply for the shape
   */
  void ApplyTransform(const tf::Transform &transform) {
    transform_ = transform * transform_;
  }

 protected:
  /** \brief The transform between the query frame and the shape */
  tf::Transform transform_;

  /**
   * \brief Checks if given vertices are valid for the given polygon type
   * @param vertices
   * @return true if polygon is valid
   */
  template<typename VertexListType, typename PolygonType>
  static bool IsValidPolygon(const VertexListType & vertices);

  /**
   * \brief check if number is nan
   * @param input
   * @return true if nan is there
   */
  static bool checkNan(double input) {
    return (std::isnan(input));
  }

  /**
   * \brief check for nans in list of things
   * @param input
   * @true if nan is there
   */
  template<typename T>
  static bool checkNan(const T & input) {
    for (size_t i = 0; i < input.size(); i++) {
      if (checkNan(input[i])) {
        return true;
      }
    }
    return false;
  }

};

/**
 * \class ca::shapes::ShapePtr
 * \brief A boost shared pointer wrapper for ca::shapes::Shape
 */
typedef boost::shared_ptr<Shape> ShapePtr;

class ShapeSet {
 public:
  /**
   * Default constructor
   */
  ShapeSet()
      : shapes_() {
  }

  virtual ~ShapeSet() {
  }

  /**
   * \brief Check if the query is in shape
   * @param pt The query point
   * @return true if in shape
   */
  virtual bool InShapeSet(const std::vector<double> &pt) const {
    for (std::size_t i = 0; i < shapes_.size(); i++)
      if (shapes_[i]->InShape(pt))
        return true;
    return false;
  }

  bool InShapeSet(const Eigen::VectorXd &pt) const {
    return InShapeSet(std::vector<double>(pt.data(), pt.data() + pt.size()));
  }

  /**
   * \brief Returns a marker array
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return
   */
  virtual visualization_msgs::MarkerArray GetMarkerArray(double r = 0,
                                                         double g = 0,
                                                         double b = 1,
                                                         double a = 0.3) {
    visualization_msgs::MarkerArray ma;
    for (std::size_t i = 0; i < shapes_.size(); i++) {
      ma.markers.push_back(shapes_[i]->GetMarker(r, g, b, a));
      ma.markers.back().id = i;
    }
    return ma;
  }

  void AddShape(boost::shared_ptr<Shape> shape) {
    shapes_.push_back(shape);
  }

  boost::shared_ptr<Shape> GetShape(size_t id) const {
    return shapes_[id];
  }

  virtual Shape::BBox GetBBox() const {
    if (shapes_.size() > 0) {
      Shape::BBox bbox = shapes_.front()->GetBBox();
      for (auto it : shapes_) {
        bbox.first = bbox.first.cwiseMin(it->GetBBox().first);
        bbox.second = bbox.second.cwiseMax(it->GetBBox().second);
      }
      return bbox;
    } else {
      return Shape::BBox();
    }
  }

  ShapePtr GetShapeContainingPoint(const Eigen::VectorXd &pt_eig) const {
    std::vector<double> pt(pt_eig.data(), pt_eig.data() + pt_eig.size());
    for (auto it : shapes_) {
      if (it->InShape(pt)) {
        return it;
      }
    }
    return ShapePtr();
  }

  size_t Size(void) const {
    return shapes_.size();
  }
  void Clear(void) {
    shapes_.clear();
  }
  std::vector<ShapePtr> shapes(void) const {
    return shapes_;
  }
 private:

  /**
   * \brief The vector of all obstacles
   */
  std::vector<ShapePtr> shapes_;
};

class ShapeWithHoles {
 public:
  ShapeWithHoles()
      : valid_area_(),
        holes_() {
  }

  virtual ~ShapeWithHoles() {
  }

  virtual bool InShapeWithHoles(const std::vector<double> &pt) const {
    if (valid_area_->InShape(pt)) {
      for (std::size_t i = 0; i < holes_.size(); i++)
        if (holes_[i]->InShape(pt))
          return false;
      return true;
    } else {
      return false;
    }
  }

  virtual visualization_msgs::MarkerArray GetMarkerArray(double r = 0,
                                                         double g = 0,
                                                         double b = 1,
                                                         double a = 0.3) const {
    visualization_msgs::MarkerArray ma;
    for (std::size_t i = 0; i < holes_.size(); i++) {
      ma.markers.push_back(holes_[i]->GetMarker(r, g, b, a));
      ma.markers.back().id = i;
    }
    ma.markers.push_back(valid_area_->GetMarker(b, g, r, a));
    ma.markers.back().id = holes_.size() + 1;
    return ma;
  }

  void SetValidArea(ShapePtr valid_area) {
    valid_area_ = valid_area;
  }

  void AddHole(boost::shared_ptr<Shape> hole) {
    holes_.push_back(hole);
  }

 private:
  ShapePtr valid_area_;
  std::vector<ShapePtr> holes_;
};

/**
 * \brief 2D Circle primitive
 */
class Circle : public Shape {
 public:
  typedef CGAL::Simple_cartesian<double> K;
  typedef K::Point_2 Point;
  typedef K::Circle_2 Circle2d;

  /**
   * \brief Create a 2D circle
   * @param x xcoordinate of circle
   * @param y ycoordinate of circle
   * @param r radius of circle
   */
  Circle(double x, double y, double r);
  virtual ~Circle() {
  }

  /**
   * \brief Gets the dimension of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 2;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_CIRCLE;
  }

  /**
   * \brief Gets the radius of the circle
   * @param radius Radius of the circle
   */
  void GetSize(double &radius);

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (2d / 3d orr nd depending on shape)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape.
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new Circle(*this);
  }  // Factory Method

  using Shape::InShape;

 private:
  /**
   * \brief The CGAL circle object
   */
  Circle2d circle_;
  /**
   * \brief Radius of the circle
   */
  double radius_;
};

/**
 * \brief 2D Sector primitive
 */
class Sector : public Shape {
 public:

  /**
   * \brief Create a 2D sector going anti-clockwise
   * @param x xcoordinate of circle
   * @param y ycoordinate of circle
   * @param r radius of circle
   * @param theta_start - start angle
   * @param theta_end - end angle
   */
  Sector(double x, double y, double r, double theta_start, double theta_end);
  virtual ~Sector() {
  }

  /**
   * \brief Gets the dimension of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 2;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_SECTOR;
  }

  /**
   * \brief Gets the radius of the sector
   * @param radius Radius of the sector
   */
  void GetSize(double &radius);

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (2d / 3d orr nd depending on shape)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape.
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new Sector(*this);
  }  // Factory Method

  using Shape::InShape;

  void set_resolution_(double resolution) {
    resolution_ = resolution;
  }

  virtual Shape::BBox GetBBox() const;

 private:
  /**
   * \brief The radius
   */
  double radius_;
  /**
   * \brief The start angle
   */
  double theta_start_;
  /**
   * \brief The end angle
   */
  double theta_end_;
  /**
   * \brief Resolution for display
   */
  double resolution_;
};

/**
 * \brief A rectangle primitive
 */
class Rectangle : public Shape {
 public:
  typedef CGAL::Simple_cartesian<double> K;
  typedef K::Point_2 Point;
  typedef K::Iso_rectangle_2 Rectangle2d;
  typedef CGAL::Bbox_2 Bbox2;

  /**
   * Creates a 2D rectangle object
   * @param xcom X coord of centre of mass of rectangle
   * @param ycom Y coord of centre of mass of rectangle
   * @param xsize X size of rectangle
   * @param ysize Y size of rectangle
   */
  Rectangle(double xcom, double ycom, double xsize, double ysize);
  virtual ~Rectangle() {
  }

  /**
   * \brief Gets the dimension of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 2;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_RECTANGLE;
  }

  /**
   * \brief Gets the size of the rectangle
   * @return A vector containing the width and height of the rectangle
   */
  void GetSize(double &width, double &height);

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (2d / 3d orr nd depending on shape)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape.
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new Rectangle(*this);
  }  // Factory Method

  using Shape::InShape;

 protected:
  /**
   * \brief The CGAL rectangle object
   */
  Rectangle2d rectangle_;

  /**
   * \brief Width of the rectangle
   */
  double width_;

  /**
   * \brief Height of the rectangle
   */
  double height_;
};

/**
 * \brief A polygon primitive
 */

class Polygon : public Shape {
 public:
  typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
  typedef Kernel::Point_2 Point_2;
  typedef CGAL::Polygon_2<Kernel> Polygon_2;
  typedef CGAL::Segment_2<Kernel> Segment_2;
  typedef CGAL::Polygon_with_holes_2<Kernel> Polygon_with_holes_2;
  typedef CGAL::Aff_transformation_2<Kernel> Transformation;
  typedef CGAL::Bbox_2 Bbox2;
  typedef CGAL::Vector_2<Kernel> Vector;
  typedef std::vector<Eigen::Vector2d, Eigen::aligned_allocator<Eigen::Vector2d> > VectorEigen2d;
  typedef std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> > VectorEigen3d;
  typedef boost::shared_ptr<Polygon_2> PolygonPtr;
  typedef std::vector<PolygonPtr> PolygonPtrVector;
  typedef Kernel::FT FT;
  typedef CGAL::Polygon_2<Kernel>::Vertex_iterator PolyVIterator;
  /**
   * \brief Create a null polygon
   */
  Polygon()
      : polygon_() {
  }

  /**
   * \brief Create a polygon with given vertices
   * @param polygon The outer polygon set of vertices
   */
  Polygon(const VectorEigen2d &polygon);
  virtual ~Polygon() {
  }

  /**
   * \brief Gets the dimension of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 2;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_POLYGON;
  }

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (2d/3d depending on the shape)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Checks if line segment lies completely in shape
   * @param pt1 list of doubles representing first endpoint of segment
   * @param pt2 list of doubles representing second endpoint of segment
   * @return true if segment is entirely inside of polygon
   */
  virtual bool InShape(const std::vector<double> &pt1,
                       const std::vector<double> & pt2) const;

  /**
   * \brief Checks if line segment lies completely in shape
   * @param pt1 floating point vector representing first endpoint of segment
   * @param pt2 floating point vector representing second endpoint of segment
   * @return true if segment is entirely inside of polygon
   */
  virtual bool InShape(const Eigen::Vector2d &pt1,
                       const Eigen::Vector2d & pt2) const;

  /**
   * \brief Checks if polygon lies completely in shape
   * @param polygon a convex polygon
   * @return true if polygon is entirely inside of this polygon
   */
  virtual bool InShape(const Polygon & polygon) const;

  /**
   * \brief Checks if line segment collides with shape
   * @param pt1 list of doubles representing first endpoint of segment
   * @param pt2 list of doubles representing second endpoint of segment
   * @return true if part of segment intersects with shape
   */
  virtual bool intersectsShape(const std::vector<double> &pt1,
                               const std::vector<double> & pt2) const;

  /**
   * \brief Checks if line segment collides with shape
   * @param pt1 floating point vector representing first endpoint of segment
   * @param pt2 floating point vector representing second endpoint of segment
   * @return true if part of segment intersects with shape
   */
  virtual bool intersectsShape(const Eigen::Vector2d &pt1,
                               const Eigen::Vector2d & pt2) const;

  /**
   * \brief Check if polygon collides with shape
   * @param polygon a convex polygon
   * @return true if part of polygon intersects with shape
   */
  virtual bool intersectsShape(const Polygon & polygon) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  /**
   * \brief A factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new Polygon(*this);
  }  //Factory Method

  virtual BBox GetBBox() const;

  VectorEigen2d GetVertices() const;

  VectorEigen2d GetUntransformedVertices() const;

  using Shape::InShape;

  void MakeClockwise();

  void MakeAntiClockwise();

  void ShiftPolygon(const Eigen::Vector2d &offset);

  friend std::ostream& operator<<(std::ostream& os, const Polygon& poly);

  const Polygon_2& polygon() const {
    return polygon_;
  }

  /**
   * \brief Checks if vertices are valid for the given polygon type
   * @param vertices
   * @return true if polygon is valid
   */
  static bool IsValidPolygon(const VectorEigen2d & vertices);

 protected:
  /**
   * \brief The outer polygon CGAL Object
   */
  Polygon_2 polygon_;
};

/**
 * \brief A polygon with hole primitive
 */
class PolygonWithHole : public Shape {
 public:
  typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
  typedef Kernel::Point_2 Point_2;
  typedef CGAL::Polygon_2<Kernel> Polygon_2;
  typedef CGAL::Polygon_with_holes_2<Kernel> Polygon_with_holes_2;
  typedef std::list<Polygon_with_holes_2> Pwh_list_2;
  typedef CGAL::Aff_transformation_2<Kernel> Transformation;
  typedef CGAL::Bbox_2 Bbox2;
  typedef CGAL::Vector_2<Kernel> Vector;
  typedef std::vector<Eigen::Vector2d, Eigen::aligned_allocator<Eigen::Vector2d> > VectorEigen2d;
  typedef std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> > VectorEigen3d;

  /**
   * \brief Create a null polygon
   */
  PolygonWithHole()
      : polygon_(),
        hole_() {
  }

  /**
   * \brief Create a polygon with a hole
   * @param polygon The outer polygon set of vertices
   * @param hole The inner hole set of vertices
   */
  PolygonWithHole(const VectorEigen2d &polygon, const VectorEigen2d &hole);
  virtual ~PolygonWithHole() {
  }

  /**
   * \brief Gets the dimension of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 2;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_POLYGON_WITH_HOLE;
  }

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (2d / 3d orr nd depending on shape)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape.
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new PolygonWithHole(*this);
  }  // Factory Method

  /**
   * \brief Move hole to x, y location
   * @param x X coordinate of hole
   * @param y Y coordinate of hole
   */
  void CentreHole(double x, double y);

  /**
   * \brief Move hole to x location keeping y constant
   * @param x X coordinate of hole
   */
  void CentreHoleX(double x);

  /**
   * \brief Move hole to y location keeping x constant
   * @param y Y coordinate of hole
   */
  void CentreHoleY(double y);

  using Shape::InShape;

  /**
   * \brief Checks if vertices are valid for the given polygon type
   * @param vertices
   * @return true if polygon is valid
   */
  static bool IsValidPolygon(const VectorEigen2d & vertices);

 protected:
  /**
   * \brief The outer polygon CGAL object
   */
  Polygon_2 polygon_;

  /**
   * \brief The inner polygon CGAL object
   */
  Polygon_2 hole_;
};

/**
 * \class ca::shapes::PolygonWithHolePtr
 * \brief A boost shared pointer wrapper for ca::shapes::Polygon
 */
typedef boost::shared_ptr<PolygonWithHole> PolygonWithHolePtr;

/**
 * \brief A 3D Cylinder primitive
 */
class Cylinder : public Shape {
 public:
  typedef CGAL::Simple_cartesian<double> K;
  typedef K::Point_2 Point2d;
  typedef K::Circle_2 Circle2d;
  typedef CGAL::Vector_3<K> Vector;

  /**
   * \brief Create a 3D cylinder
   * @param x xcoordinate of centre of mass of cylinder
   * @param y ycoordinate of centre of mass of cylinder
   * @param z zcoordinate of centre of mass of cylinder
   * @param r radius of cylinder
   * @param h height of cylinder
   * @param ax axis of cylinder as a unit vector of doubles
   */
  Cylinder(double x, double y, double z, double r, double h,
           const std::vector<double> &ax = {0, 0, 1});
  virtual ~Cylinder() {
  }

  /**
   * \brief Gets the dimnesion of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 3;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_CYLINDER;
  }

  /**
   * \brief Gets the radius of the circle
   * @param radius Radius of the circle
   */
  void GetSize(double &radius);

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (3d)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new Cylinder(*this);
  }  //Factory Method

  using Shape::InShape;

 protected:
  /**
   * \brief The height of cylinder
   */
  double height_;
  /**
   * \brief The radius of cylinder
   */
  double radius_;

};

/**
 * \brief A 3D Cylinder primitive
 */
class CylinderExpanded : public Shape {
 public:
  typedef CGAL::Simple_cartesian<double> K;
  typedef K::Point_2 Point2d;
  typedef K::Circle_2 Circle2d;
  typedef CGAL::Vector_3<K> Vector;

  /**
   * \brief Create a 3D cylinder
   * @param x xcoordinate of centre of mass of cylinder
   * @param y ycoordinate of centre of mass of cylinder
   * @param z zcoordinate of centre of mass of cylinder
   * @param r radius of cylinder
   * @param h height of cylinder
   * @param ax axis of cylinder as a unit vector of doubles
   */
  CylinderExpanded(double x, double y, double z, double r, double exp, double h,
                   const std::vector<double> &ax);
  virtual ~CylinderExpanded() {
  }

  /**
   * \brief Gets the dimnesion of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 3;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_CYLINDER;
  }

  /**
   * \brief Gets the radius of the circle
   * @param radius Radius of the circle
   */
  void GetSize(double &radius);

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (3d)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new CylinderExpanded(*this);
  }  //Factory Method

  using Shape::InShape;

 protected:
  /**
   * \brief The height of cylinder
   */
  double height_;
  /**
   * \brief The radius of cylinder
   */
  double radius_;

  double exp_;

};

/**
 * \brief Cuboid
 */
class Cuboid : public Rectangle {
 public:

  Cuboid(double xcom, double ycom, double xsize, double ysize, double z_lower,
         double z_upper);

  virtual ~Cuboid() {
  }

  /**
   * \brief Gets the dimension of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 3;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_CUBOID;
  }

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (2d / 3d orr nd depending on shape)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape.
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  shapes::PolygonDepth GetMsg() const;

  void FromMsg(const shapes::PolygonDepth &msg);

  virtual BBox GetBBox() const;


  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new Cuboid(*this);
  }  // Factory Method

  using Shape::InShape;

  double z_lower() const {
    return z_lower_;
  }

  double z_upper() const {
    return z_upper_;
  }

 protected:
  /**
   * \brief The lower limit of the z
   */
  double z_lower_;
  /**
   * \brief The upper limit of the z
   */
  double z_upper_;

  Eigen::Vector3d lb_, ub_;
};

/**
 * \brief A 3D Pyramid primitive
 */
class Pyramid : public Shape {
 public:

  Pyramid(const Eigen::Vector2d &base_centre, double z_low, double z_high,
          double x_size_base, double y_size_base, double x_size_top,
          double y_size_top);
  virtual ~Pyramid() {
  }

  /**
   * \brief Gets the dimnesion of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 3;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_PYRAMID;
  }

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (3d)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new Pyramid(*this);
  }  //Factory Method

  using Shape::InShape;

 private:
  Eigen::Vector2d base_centre_;
  double z_low_, z_high_, x_size_base_, y_size_base_, x_size_top_, y_size_top_;

};

class PolygonDepth : public Polygon {
 public:
  /**
   * \brief Construct empty class
   */
  PolygonDepth()
      : z_lower_(),
        z_upper_() {
  }

  /**
   * \brief Construct polygon with hole
   * @param polygon The outer polygon
   * @param hole The inner hole
   * @param depth_lower The bottom layer of depth
   * @param depth_upper The top layer of depth
   */
  PolygonDepth(const VectorEigen2d &polygon, double z_lower, double z_upper);

  virtual ~PolygonDepth() {
  }

  /**
   * \brief Gets the dimension of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 3;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_POLYGON_DEPTH;
  }

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (2d / 3d orr nd depending on shape)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Checks if line segment lies completely in shape
   * @param pt1 list of doubles representing first endpoint of segment
   * @param pt2 list of doubles representing second endpoint of segment
   * @return true if segment is entirely inside of polygon
   */
  virtual bool InShape(const std::vector<double> &pt1,
                       const std::vector<double> & pt2) const;

  /**
   * \brief Checks if line segment lies completely in shape
   * @param pt1 floating point vector representing first endpoint of segment
   * @param pt2 floating point vector representing second endpoint of segment
   * @return true if segment is entirely inside of polygon
   */
  virtual bool InShape(const Eigen::Vector3d &pt1,
                       const Eigen::Vector3d & pt2) const;

  /**
   * \brief Checks if polygon lies completely in shape
   * @param polygon a convex polygon
   * @return true if polygon is entirely inside of this polygon
   */
  virtual bool InShape(const PolygonDepth & polygon) const;

  /**
   * \brief Checks if line segment lies within the shape
   * @param pt1 list of doubles representing first endpoint of segment
   * @param pt2 list of doubles representing second endpoint of segment
   * @return true if part of segment intersects with shape
   */
  virtual bool intersectsShape(const std::vector<double> &pt1,
                               const std::vector<double> & pt2) const;

  /**
   * \brief Checks if line segment lies within the shape
   * @param pt1 floating point vector representing first endpoint of segment
   * @param pt2 floating point vector representing second endpoint of segment
   * @return true if part of segment intersects with shape
   */
  virtual bool intersectsShape(const Eigen::Vector3d &pt1,
                               const Eigen::Vector3d & pt2) const;

  /**
   * \brief Check if polygon collides with shape
   * @param polygon a convex polygon
   * @return true if part of polygon intersects with shape
   */
  virtual bool intersectsShape(const PolygonDepth & polygon) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape.
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  shapes::PolygonDepth GetMsg() const;

  void FromMsg(const shapes::PolygonDepth &msg);

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new PolygonDepth(*this);
  }  // Factory Method

  virtual Shape::BBox GetBBox() const;

  using Shape::InShape;

  double z_lower() const {
    return z_lower_;
  }

  double z_upper() const {
    return z_upper_;
  }

  void ShiftPolygonDepth(const Eigen::Vector3d &offset);

 protected:
  /**
   * \brief The lower limit of the z
   */
  double z_lower_;
  /**
   * \brief The upper limit of the z
   */
  double z_upper_;
};

/**
 * \brief A polygon hole with depth. It is an extension of polygon with hole class.
 * It adds a layer of depth to the class.
 */
class PolygonWithHoleDepth : public PolygonWithHole {
 public:
  /**
   * \brief Construct empty class
   */
  PolygonWithHoleDepth()
      : depth_lower_(),
        depth_upper_() {
  }

  /**
   * \brief Construct polygon with hole
   * @param polygon The outer polygon
   * @param hole The inner hole
   * @param depth_lower The bottom layer of depth
   * @param depth_upper The top layer of depth
   */
  PolygonWithHoleDepth(const VectorEigen2d &polygon, const VectorEigen2d &hole,
                       double depth_lower, double depth_upper);

  virtual ~PolygonWithHoleDepth() {
  }

  /**
   * \brief Gets the dimension of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 3;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_POLYGON_WITH_HOLE_DEPTH;
  }

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (2d / 3d orr nd depending on shape)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape.
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new PolygonWithHoleDepth(*this);
  }  // Factory Method

  using Shape::InShape;

 protected:
  /**
   * \brief The lower limit of the depth
   */
  double depth_lower_;
  /**
   * \brief The upper limit of the depth
   */
  double depth_upper_;
};

class ObliqueRectangularPrism : public Shape {
 public:
  /**
   * \brief Create an oblique rectangular prism
   * @param pivot1 coordinate of the first pivot
   * @param pivot2 coordinate of the second pivot
   */
  ObliqueRectangularPrism(const Eigen::Vector3d &pivot1,
                          const Eigen::Vector3d &pivot2, double pos_y,
                          double neg_y, double pos_z, double neg_z);
  virtual ~ObliqueRectangularPrism() {
  }

  /**
   * \brief Gets the dimnesion of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 3;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_OBLIQUE_RECTANGULAR_PRISM;
  }

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (3d)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new ObliqueRectangularPrism(*this);
  }  //Factory Method

  virtual Shape::BBox GetBBox() const;

  using Shape::InShape;

 private:
  /**
   * \brief The two pivots
   */
  Eigen::Vector3d dir_;
  /**
   * \brief The parameters of the slice
   */
  double pos_y_, neg_y_, pos_z_, neg_z_;
};

class SectorDepth : public Sector {
 public:

  /**
   * \brief Construct sector with depth
   * @param polygon sector
   * @param depth_lower The bottom layer of depth
   * @param depth_upper The top layer of depth
   */
  SectorDepth(double x, double y, double r, double theta_start,
              double theta_end, double depth_lower, double depth_upper);

  virtual ~SectorDepth() {
  }

  /**
   * \brief Gets the dimension of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 3;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_SECTOR_DEPTH;
  }

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (2d / 3d orr nd depending on shape)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape.
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new SectorDepth(*this);
  }  // Factory Method

  using Shape::InShape;

  double depth_lower() {
    return depth_lower_;
  }

  double depth_upper() {
    return depth_upper_;
  }

  virtual Shape::BBox GetBBox() const;


 protected:
  /**
   * \brief The lower limit of the depth
   */
  double depth_lower_;
  /**
   * \brief The upper limit of the depth
   */
  double depth_upper_;
};

/**
 * \brief A polygon with hole primitive
 */
class PolygonWithHolesDepth : public Shape {
 public:
  /**
   * \brief Create a null polygon
   */
  PolygonWithHolesDepth()
      : outer_(),
        holes_() {
  }

  /**
   * \brief Create a polygon with a hole
   * @param polygon The outer polygon set of vertices
   * @param hole The inner hole set of vertices
   */
  PolygonWithHolesDepth(const PolygonDepth &outer,
                        const std::vector<PolygonDepth> &holes);
  virtual ~PolygonWithHolesDepth() {
  }

  /**
   * \brief Gets the dimension of the shape
   * @return dimension of the shape
   */
  virtual double GetDimension() const {
    return 3;
  }

  /**
   * \brief Gets the shape type
   * @return type of shape
   */
  virtual ShapeType GetShapeType() {
    return SHAPE_POLYGON_WITH_HOLES_DEPTH;
  }

  /**
   * \brief Checks if a point lies within the shape
   * @param pt : vector of doubles (2d / 3d orr nd depending on shape)
   * @return true if inside the shape and false otherwise
   */
  virtual bool InShape(const std::vector<double> &pt) const;

  /**
   * \brief Creates a visualization_msgs::Marker for the shape.
   * @param r Red
   * @param g Green
   * @param b Blue
   * @param a Alpha
   * @return A marker
   */
  virtual visualization_msgs::Marker GetMarker(double r = 1, double g = 0,
                                               double b = 0, double a = 1);

  /**
   * \brief  A Factory method for generating clones
   * @return A shape pointer
   */
  virtual Shape *Clone() {
    return new PolygonWithHolesDepth(*this);
  }  // Factory Method

  void SetOuter(const PolygonDepth &outer) {
    outer_ = outer;
  }

  void AddHole(const PolygonDepth &hole) {
    holes_.push_back(hole);
  }

  shapes::PolygonWithHolesDepth GetMsg() const;

  void FromMsg(const shapes::PolygonWithHolesDepth &msg);

  const PolygonDepth& outer() const {
    return outer_;
  }
  PolygonDepth& outer() {
    return outer_;
  }

  const std::vector<PolygonDepth>& holes() const {
    return holes_;
  }
  std::vector<PolygonDepth>& holes() {
    return holes_;
  }

  virtual Shape::BBox GetBBox() const {
    return outer_.GetBBox();
  }


  using Shape::InShape;

 protected:
  /**
   * \brief The outer polygon CGAL object
   */
  PolygonDepth outer_;

  /**
   * \brief The inner polygon CGAL object
   */
  std::vector<PolygonDepth> holes_;
};

}  // namespace ca

#endif  // SHAPES_H_
