## Required for CGAL
set(ORIG_CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE})
if((CMAKE_BUILD_TYPE STREQUAL "")  OR (CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo"))
  set(CMAKE_BUILD_TYPE "Debug")
endif()
find_package(CGAL REQUIRED COMPONENTS Core)
include(${CGAL_USE_FILE})
set(CMAKE_BUILD_TYPE ${ORIG_CMAKE_BUILD_TYPE})
include_directories(${CGAL_INCLUDE_DIRS})

## Required for this library
set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}")